@extends('layouts.limitless.index')
@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        <h5 class="panel-title">รายละเอียดการเคลม <a class="heading-elements-toggle"><i class="icon-more"></i></a>
        </h5>
        <div class="heading-elements">
            <a class="btn btn-primary" href="{{ url('/backend/orders') }}"> Back</a>
        </div>
    </div>
    <div class="panel-body">
        {{-- [id] => 3
        [order_date] => 09/02/2020
        [order_time] => 16:55:41
        [name_surename] => aroon sing
        [tel] => 0851670472
        [address] => 77/160 soi Ekachai 46
        [districts] => บางบอน บางบอน กรุงเทพมหานคร
        [zip_code] => 10150
        [product] => T-shirt JT-308 Blue
        [payment] => โอน
        [total_price] => 1060
        [purchase_channel] => FACEBOOK
        [profile] => testt
        [created_at] => 2020-02-09 16:21:23
        [updated_at] => 2020-02-09 16:55:41
        [deleted_at] =>  --}}
        <fieldset class="content-group">
            <div class="form-group">
                <label class="control-label col-xs-3">เลขที่สั่งซื้อ</span></label>
                <div class="col-xs-9">
                    <div class="form-control-static">{{$data->id}}</div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-xs-3">วันที่สั่งซื้อ/เวลาสั่งซื้อ</span></label>
                <div class="col-xs-9">
                    <div class="form-control-static">{{$data->order_date}} {{$data->order_time}}</div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-xs-3">ชื่อสินค้า</span></label>
                <div class="col-xs-9">
                    <div class="form-control-static">{{$data->product}}</div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-xs-3">ชื่อผู้สั่งซื้อ</span></label>
                <div class="col-xs-9">
                    <div class="form-control-static">{{$data->name_surename}}</div>
                </div>
            </div>

        </fieldset>
        <div class="">
            <table class="table table-bordered table-xxs" id="test-datatable">
                <thead>
                    <tr class="bg-grey-600">
                        <th>Claim ID</th>
                        <th>DATE</th>
                        <th>TIME</th>
                        <th>หัวข้อเคลม</th>
                        <th>รายละเอียดการเคลม</th>
                        <th>ชื่อผู้เคลม</th>
                        <th>เบอร์โทร</th>
                        <th>ที่อยู่</th>
                        <th>ตำบล อำเภอ จังหวัด</th>
                        <th>รหัสไปรษณี</th>
                        <th>ยอดเงินรวม</th>

                        <th class="text-center">Actions</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif
<script>
    $(function() {
         // Table setup
        // ------------------------------

        // Setting datatable defaults
        $.extend( $.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                searchPlaceholder: 'Type to filter...',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
            },
        });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    // Warning alert
        function deleteRecord(id) {
            confirmDelete(id);
        }
        $('.deleteRecord').on('click', function() {
            var id = $(this).attr('rel');
            confirmDelete(id);
        });

        $('#test-datatable').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ url('/backend/claim-orders/data')}}/{{$order_id}}",
        "columns": [
            {"data":'id'},
            {"data":'order_date'},
            {"data":'order_time'},
            {"data":'claim_name'},
            {"data":'description'},
            {"data":'name_surename'},
            {"data":'tel'},
            {"data":'address'},
            {"data": 'districts'},
            {"data":'zip_code'},
            {"data": 'total_price', render: $.fn.dataTable.render.number( ',', '.', 0 ) },
            {"data": null}
        ],
        buttons: [
            {
                text: 'สร้างใบเคลม',
                className: 'btn btn-success heading-btn',
                action: function(e, dt, node, config) {
                    location.href = "{{ route('claim-orders.create',[$order_id]) }}";

                }
            }
        ],
        columnDefs: [
                {className: "text-center", "targets": [0, 1, 2, 3, 4, 5, 6, 7, 8, 10]},
                {className: "text-right", "targets": [9]},
                {
                    "render": function (data, type, row, meta) {
                        dataReturn = '';
                        // dataReturn += '<a href="{{ url('backend/orders')}}/' + data.id + '/edit" class="label bg-green"><i class="icon-pencil7 position-left"></i>Edit</a>&nbsp;';
                        // dataReturn += '<a class="label label-danger deleteRecord" onclick="deleteRecord('+ data.id +')" href="javascript:" rel="'+ data.id +'">Delete</a>';
                        dataReturn += '<ul class="icons-list">';
						dataReturn += '	<li class="dropdown">';
						dataReturn += '		<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">';
						dataReturn += '			<i class="icon-cog7"></i>';
						dataReturn += '			<span class="caret"></span>';
						dataReturn += '		</a>';

						dataReturn += '		<ul class="dropdown-menu dropdown-menu-solid">';
                        dataReturn += '			<li><a href="{{ url('backend/claim-orders')}}/' + data.id + '/edit"><i class="icon-pencil7"></i>แก้ไข</a></li>';
                        dataReturn += '			<li><a href="javascript:" onclick="deleteRecord('+ data.id +')"><i class="icon-bin"></i>ลบ</a></li>';

						dataReturn += '		</ul>';
						dataReturn += '	</li>';
						dataReturn += '</ul>';

                        return dataReturn;
                    },
                    "targets": 11
                },
            ],
    });

    });

    function deleteRecord(id) {
            confirmDelete(id);
    }

    function confirmDelete(id) {
            swal({
                title: "Confirm Delete",
                text: "Are you sure you want to delete this Order?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }, function (isConfirm) {
                if (!isConfirm) return;
                $.ajax({
                    url : "{{ url('backend/orders')}}" + '/' + id,
                    type : "POST",
                    data : {'_method' : 'DELETE'},
                    success: function () {
                        swal({title: "Success", text: "Order has been deleted! \nClick 'Ok' to refresh page.", type: "success"},
                            function(){
                                location.reload();
                            }
                        );
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        swal("Error!", "Please try again", "error");
                    }
                });
            });
    }
</script>

@endsection
