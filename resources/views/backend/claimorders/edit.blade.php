@extends('layouts.limitless.index')
@section('content')

<div class="row">
    <div class="col-md-6">
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        {!! Form::model($claim, ['method' => 'PATCH','class'=>'form-horizontal','route' => ['claim-orders.update',
        $claim->id]]) !!}
        <input type="hidden" value="{{ $claim->order_id}}" name="order_id">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h5 class="panel-title">แก้ไขใบเคลม<a class="heading-elements-toggle"><i class="icon-more"></i></a>
                </h5>
                <div class="heading-elements">
                    <a class="btn btn-primary" href="{{ url('backend/claim-orders')}}/{{$claim->order_id}}"> Back</a>
                </div>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-lg-3 control-label">หัวข้อเคลม:<span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        {!! Form::text('claim_name', null, array('placeholder' => 'input validation','class' =>
                        'form-control','required'=>'required')) !!}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label">รายละเอียดการเคลม:<span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <textarea rows="2" cols="5" name="description" id="description" class="form-control"
                            placeholder="Default textarea" aria-required="true"
                            aria-invalid="true">{{ $claim->description }}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label">ชื่อ-นามสกุล:<span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        {!! Form::text('name_surename', null, array('placeholder' => 'input validation','class' =>
                        'form-control','required'=>'required')) !!}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label">เบอร์โทรศัพท์<span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        {!! Form::text('tel', null, array('placeholder' => 'input validation','class' =>
                        'form-control','required'=>'required')) !!}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label">ที่อยู่:<span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <textarea rows="2" cols="5" name="address" id="address" class="form-control" required="required"
                            placeholder="Default textarea" aria-required="true"
                            aria-invalid="true">{{ $claim->address }}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label">ตำบล:<span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <input type="text" value="{{ $claim->districts }}" name="districts" id="district"
                            class="form-control" required="required">

                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label">อำเภอ:<span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <input type="text" value="{{ $claim->amphures }}" name="amphures" id="amphoe"
                            class="form-control" required="required">

                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label">จังหวัด:<span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <input type="text" value="{{ $claim->provinces }}" name="provinces" id="province"
                            class="form-control" required="required">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-3">รหัสไปรษณี <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <input type="text" value="{{ $claim->zipcode }}" name="zipcode" id="zipcode"
                            class="form-control" required="required">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-3">ยอดชำระ:<span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <input type="text" value="{{ $claim->total_price }}" name="total_price" id=""
                            class="form-control" required="required" placeholder="input validation">
                    </div>
                </div>
                <div class="text-right">
                    <button type="submit" class="btn btn-primary">Submit form <i
                            class="icon-arrow-right14 position-right"></i></button>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>
<script>
    $(document).ready(function(){
        $.Thailand({
            $district: $('#district'), // input ของตำบล
            $amphoe: $('#amphoe'), // input ของอำเภอ
            $province: $('#province'), // input ของจังหวัด
            $zipcode: $('#zipcode'), // input ของรหัสไปรษณีย์
        });
    });
</script>

@endsection
