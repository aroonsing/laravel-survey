@extends('layouts.limitless.index')

@section('content')
<div class="row">
    <div class="col-md-6">'
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
            </ul>
        </div>
        @endif
        {!! Form::model($permission, ['method' => 'PATCH','class'=>'form-horizontal','route' => ['permissions.update', $permission->id]]) !!}
        <div class="panel panel-default">
                <div class="panel-heading">
                    <h5 class="panel-title">Edit Role<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
                    <div class="heading-elements">
                            <a class="btn btn-primary" href="{{ route('permissions.index') }}"> Back</a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-lg-3 control-label">Name:</label>
                        <div class="col-lg-9">
                                {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
                        </div>
                    </div>
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary">Submit form <i class="icon-arrow-right14 position-right"></i></button>
                    </div>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
</div>

@endsection