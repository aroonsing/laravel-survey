@extends('layouts.limitless.index')
@section('content')

<div class="row">
    <div class="col-md-6">
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        {!! Form::open(array('route' => 'working-days.store','method'=>'POST','class'=>'form-horizontal')) !!}

        <div class="panel panel-default">
            <div class="panel-heading">
                <h5 class="panel-title">สร้างวันทำการ<a class="heading-elements-toggle"><i class="icon-more"></i></a>
                </h5>
                <div class="heading-elements">
                    <a class="btn btn-primary" href="{{ route('working-days.index') }}"> Back</a>
                </div>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label class="control-label col-lg-3">วันทำการวันที่:<span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <input class="form-control" type="date" name="date_working" required='required'>
                    </div>
                </div>
                <div class="text-right">
                    <button type="submit" class="btn btn-primary">Submit form <i
                            class="icon-arrow-right14 position-right"></i></button>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>
<script>
    $(document).ready(function(){
        // showProvinces();
        $.Thailand({
            $district: $('#district'), // input ของตำบล
            $amphoe: $('#amphoe'), // input ของอำเภอ
            $province: $('#province'), // input ของจังหวัด
            $zipcode: $('#zipcode'), // input ของรหัสไปรษณีย์
        });
    });
    /*function showProvinces(){
         $.ajax({
                type:'get',
                url:'{{ url('/') }}/api/province',
                success:function(resp){
                    $("#input_province").empty();
                    $("#input_province").append(
                        $('<option></option>')
                        .attr("value", "")
                        .html("กรุณาเลือกจังหวัด")
                    );
                    for(var i=0; i<resp.length; i++){
                    $("#input_province").append(
                        $('<option></option>')
                        .attr("value", ""+resp[i].id)
                        .html(""+resp[i].name_th)
                    );
                    }
                    showAmphoes();
                },error:function(){
                    alert("error");

                }
            });
    }*/

    /*function showAmphoes(){
        //INPUT
        var province_id = $("#input_province").val();
        if(province_id == ''){
            $("#input_amphoe").empty();
            $("#input_amphoe").append($('<option></option>').attr("value", "").html("กรุณาเลือกอำเภอ"));
            $("#input_district").empty();
            $("#input_district").append($('<option></option>').attr("value", "").html("กรุณาเลือกตำบล"));
            $("#zipcode").val('');
            return;
        }
        $.ajax({
            type:'get',
            url:"{{ url('/') }}/api/province/"+province_id+"/amphoe",
            success:function(resp){
                //console.log(resp);
                $("#input_amphoe").empty();
                $("#input_amphoe").append(
                        $('<option></option>')
                        .attr("value", "")
                        .html("กรุณาเลือกอำเภอ")
                    );
                for(var i=0; i<resp.length; i++){
                $("#input_amphoe").append(
                    $('<option></option>')
                    .attr("value", ""+resp[i].id)
                    .html(""+resp[i].name_th)
                );
                }
                showDistricts();
            },error:function(){
                alert("error");

            }
        });
    }*/

    /*function showDistricts(){

     //INPUT
     var amphoe_id = $("#input_amphoe").val();
        if(amphoe_id == ''){
            $("#input_district").empty();
            $("#input_district").append($('<option></option>').attr("value", "").html("กรุณาเลือกตำบล"));
            $("#zipcode").val('');
            return;
        }
        $.ajax({
            type:'get',
            url:"{{ url('/') }}/api/amphoe/"+amphoe_id+"/district",
            success:function(resp){
                //console.log(result);
                $("#input_district").empty();
                $("#input_district").append(
                        $('<option></option>')
                        .attr("value", "")
                        .html("กรุณาเลือกตำบล")
                    );
                for(var i=0; i<resp.length; i++){
                $("#input_district").append(
                    $('<option></option>')
                    .attr("value", ""+resp[i].id)
                    .attr("data-zipcode", ""+resp[i].zip_code)
                    .html(""+resp[i].name_th)
                );
                }
                showZipcode();
            },error:function(){
                alert("error");

            }
        });
    }*/

    /*function showZipcode(){
    //INPUT
    var zip_code = $("#input_district option:selected").attr('data-zipcode');
    $("#zipcode").val(zip_code);

    }*/

    function showProductAtttributes(){
         //INPUT
        var product_id = $("#product_id").val();
        if(product_id == ''){
            $("#products_attributes_id").empty();
            $("#products_attributes_id").append($('<option></option>').attr("value", "").html("เลือก"));
            return;
        }
        $.ajax({
            type:'get',
            url:"{{ url('/') }}/api/products_attributes/"+product_id,
            success:function(resp){
                //console.log(resp);
                $("#products_attributes_id").empty();
                $("#products_attributes_id").append(
                        $('<option></option>')
                        .attr("value", "")
                        .html("เลือก")
                    );
                for(var i=0; i<resp.length; i++){
                $("#products_attributes_id").append(
                    $('<option></option>')
                    .attr("value", ""+resp[i].id)
                    .html(""+resp[i].color)
                );
                }
            },error:function(){
                alert("error");

            }
        });
    }

</script>

@endsection
