@extends('layouts.limitless.index')
@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        <h5 class="panel-title">ระบบจัดการวันทำการ <a class="heading-elements-toggle"><i class="icon-more"></i></a>
        </h5>
        <div class="heading-elements">
        </div>
    </div>
    <div class="panel-body">
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <div class="table-responsive">
            <table class="table table-bordered table-xxs" id="test-datatable">
                <thead>
                    <tr class="bg-grey-600">
                        <th>ID</th>
                        <th>วันที่ทำการ</th>
                        <th>สถานะ เปิด-ปิด</th>
                        <th class="text-center">Actions</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif
<script>
    $(function() {
         // Table setup
        // ------------------------------

        // Setting datatable defaults
        $.extend( $.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                searchPlaceholder: 'Type to filter...',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
            },
        });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#test-datatable').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ url('backend/working-days/data')}}",
        "columns": [
            {"data":'id'},
            {"data":'date_working'},
            {"data": null},
            {"data": null}
        ],
        buttons: [
            {
                text: 'สร้างวันทำการ',
                className: 'btn btn-success heading-btn',
                action: function(e, dt, node, config) {
                    location.href = "{{ route('working-days.create') }}";

                }
            }
        ],
        order: [[ 0, "desc" ]],
        columnDefs: [
                {className: "text-center", "targets": [0, 1,3 ]},
                {className: "text-left", "targets": [2]},
                {
                    "render": function (data, type, row, meta) {
                        dataReturn = '';
                        if(data.status_closed == 0){
                            dataReturn += '<span class="label label-success">เปิด</span>';
                        }else{
                            dataReturn += '<span class="label label-danger">ปิด</span>';
                        }

                        return dataReturn;
                    },
                    "targets": 2
                },
                {
                    "render": function (data, type, row, meta) {
                        dataReturn = '';
                        if(data.status_closed == 0){
                        dataReturn += '<a href="javascript:" onclick="confirmCloseWorkingDays('+ data.id +')" class="label bg-danger"><i class="icon-pencil7 position-left"></i>ปิดวันทำการ</a>&nbsp;';
                        }
                        return dataReturn;
                    },
                    "targets": 3
                },
            ],
    });

    });

    function confirmCloseWorkingDays(id) {
            swal({
                title: "ยืนยันการปิดวันทำการ",
                // text: "Are you sure you want to delete this Senders?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, ยืนยันการปิด!",
                cancelButtonText: "ยกเลิก",
                closeOnConfirm: false
            }, function (isConfirm) {
                if (!isConfirm) return;
                $.ajax({
                    url : "{{ url('backend/working-days')}}" + '/' + id,
                    type : "POST",
                    data : {'_method' : 'PATCH'},
                    success: function () {
                        swal({title: "Success", text: "วันทำการของคุณได้ปิดเรียบร้อย! \nคลิกปุ่ม 'Ok' เพื่อรีเฟรชหน้า.", type: "success"},
                            function(){
                                location.reload();
                            }
                        );
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        swal("Error!", "Please try again", "error");
                    }
                });
            });
    }
</script>

@endsection
