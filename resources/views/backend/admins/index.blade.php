@extends('layouts.limitless.index')
@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        <h5 class="panel-title">Admins Management<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
        <div class="heading-elements">
            {{-- @can('user-create') --}}
            <a class="btn btn-success heading-btn" href="{{ route('admins.create') }}"> Create New Admin</a>
            {{-- @endcan --}}
        </div>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-bordered table-xxs">
                <thead>
                    <tr class="bg-grey-600">
                        <th>#</th>
                        <th>Name</th>
                        <th>Username</th>
                        <th>Roles</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $key => $user)
                    <tr>
                        <td>{{ ++$i }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->username }}</td>
                        <td>
                            @if(!empty($user->getRoleNames()))
                            @foreach($user->getRoleNames() as $v)
                            <span class="label label-flat border-success text-success-600">{{ $v }}</span>
                            @endforeach
                            @endif
                        </td>
                        <td>
                            <a href="{{ route('admins.show',$user->id) }}" class="label bg-primary"><i
                                    class="icon-file-text position-left"></i>Show</a>
                            {{-- @can('user-edit') --}}
                            <a href="{{ route('admins.edit',$user->id) }}" class="label bg-green"><i
                                    class="icon-pencil7 position-left"></i>Edit</a>
                            {{-- @endcan   --}}
                            @if (Auth::user()->id != $user->id)
                            <a class="label label-danger deleteRecord" href="javascript:"
                                rel="{{$user->id}}">Delete</a>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="panel-footer"><a class="heading-elements-toggle"><i class="icon-more"></i></a>
        <div class="heading-elements">
            <span class="heading-text text-semibold">Pagination:</span>
            {!! $data->links('layouts.limitless.partials.pagination.default') !!}

        </div>
    </div>
</div>
@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif
<script>
    $(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    // Warning alert
        $('.deleteRecord').on('click', function() {
            var id = $(this).attr('rel');
            confirmDelete(id);
        });


    });

    function confirmDelete(id) {
            swal({
                title: "Confirm Delete",
                text: "Are you sure you want to delete this Admin ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }, function (isConfirm) {
                if (!isConfirm) return;
                $.ajax({
                    url : "{{ url('backend/admins')}}" + '/' + id,
                    type : "POST",
                    data : {'_method' : 'DELETE'},
                    success: function () {
                        swal({title: "Success", text: "Admin has been deleted! \nClick 'Ok' to refresh page.", type: "success"},
                            function(){
                                location.reload();
                            }
                        );
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        swal("Error!", "Please try again", "error");
                    }
                });
            });
    }
</script>

@endsection
