@extends('layouts.limitless.index')
@section('content')
<div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h5 class="panel-title">ระบบจัดการ Stock <a class="heading-elements-toggle"><i class="icon-more"></i></a>
            </h5>
            <div class="heading-elements">
                <a class="btn btn-primary" href="{{ route('products.index') }}"> Back</a>
            </div>
        </div>
        <div class="panel-body">
            <fieldset class="content-group">
                <input type="hidden" name="product_id" value="{{$products_attributes->id}}">
                <div class="form-group">
                    <label class="control-label col-xs-1">ชื่อสินค้า :</label>
                    <label class="control-label col-xs-2">
                        {{$products_attributes->product_name}}
                        {{$products_attributes->color}}
                    </label>
                </div>
            </fieldset>
            <div class="table-responsive">
                <table class="table table-bordered table-xxs">
                    <thead>
                        <tr class="bg-grey-600">
                            <th class="col-lg-2">id</th>
                            <th class="col-lg-2">นำเข้าวันที่</th>
                            <th class="col-lg-2">จำนวนสินค้าที่เพิ่ม</th>
                            <th class="col-lg-2">จำนวนสินค้าคงเหลือ</th>
                            <th class="col-lg-2">ราคาทุน</th>
                            <th class=" col-lg-2 text-center">ปรับ Stock</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                        $product_name_old = '';
                        @endphp
                        @foreach ($data as $key => $product)
                        <tr>
                            <td class="text-center">{{$product->id}}</td>
                            <td class="text-center">{{$product->created_at}}</td>
                            <td class="text-right">{{$product->stock_add}}</td>
                            <td class="text-right">{{$product->stock_balance}}</td>
                            {{-- <td class="text-center">{{$product->price_cost}}</td> --}}
                            <td>
                                <a href="#" data-type="text" data-pk="{{$product->id}}" data-title="ปรับ Stock คงเหลือ"
                                    class="editable-price_cost editable-click">{{$product->price_cost}}</a>
                            </td>
                            <td>
                                <a href="#" data-type="text" data-pk="{{$product->id}}" data-title="ปรับ Stock คงเหลือ"
                                    class="editable-stock_balance editable-click">{{$product->stock_balance}}</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
    @endif
</div>
<script>
    $(function() {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        // Editable
        // ------------------------------

        // Change defaults
        $.fn.editable.defaults.highlight = false;
        $.fn.editable.defaults.mode = 'popup';
        $.fn.editableform.template = '<form class="editableform">' +
            '<div class="control-group">' +
            '<div class="editable-input"></div> <div class="editable-buttons"></div>' +
            '<div class="editable-error-block"></div>' +
            '</div> ' +
            '</form>';
        $.fn.editableform.buttons =
            '<button type="submit" class="btn btn-info btn-icon editable-submit"><i class="icon-check"></i></button>' +
            '<button type="button" class="btn btn-default btn-icon editable-cancel"><i class="icon-x"></i></button>';


        // Initialize
        $('.editable-price_cost').editable({
            name: 'price_cost',
            url: '{{route("stocks.manage.update")}}',
            type: 'text',
            pk: 1,
            success: function(response, newValue) {
                if(response == ''){
                    swal({title: "Success", text: "ปรับ Stock เรียบร้อย! \nClick 'Ok' to refresh page.", type: "success"},
                            function(){
                                location.reload();
                            }
                        );
                }else{
                    swal({title: "Error!", text: response, type: "error"},
                            function(){
                                location.reload();
                            }
                        );
                }
            }
        });

        $('.editable-stock_balance').editable({
            name: 'stock_balance',
            url: '{{route("stocks.manage.update")}}',
            type: 'text',
            pk: 1,
            success: function(response, newValue) {
                if(response == ''){
                    swal({title: "Success", text: "ปรับ Stock เรียบร้อย! \nClick 'Ok' to refresh page.", type: "success"},
                            function(){
                                location.reload();
                            }
                        );
                }else{
                    swal({title: "Error!", text: response, type: "error"},
                            function(){
                                location.reload();
                            }
                        );
                }
            }
        });
    });

</script>

@endsection
