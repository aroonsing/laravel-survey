@extends('layouts.limitless.index')

@section('content')
<div class="row">
    <div class="col-md-6">
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        {!! Form::model($data, ['method' => 'PATCH','class'=>'form-horizontal','route' => ['stocks.update', $data->id]])
        !!}
        <div class="panel panel-default">
            <div class="panel-heading">
                <h5 class="panel-title">เพิ่ม Stock<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
                <div class="heading-elements">
                    <a class="btn btn-primary" href="{{ route('products.index') }}"> Back</a>
                </div>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-lg-3 control-label">ชื่อสินค้า:</label>
                    <div class="col-lg-9">
                        <div class="form-control-static">{{$data->product_name}}</div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label">สี:</label>
                    <div class="col-lg-9">
                        <div class="form-control-static">{{$data->color}}</div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label">จำนวน Stock คงเหลือ:</label>
                    <div class="col-lg-9">
                        <div class="form-control-static">{{$data->stock}}</div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label">จำนวน Stock ที่นำเข้า:</label>
                    <div class="col-lg-9">
                        <input class="form-control" type="number" name="stock_add">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label">ราคาทุนของ Stock ที่นำเข้า:</label>
                    <div class="col-lg-9">
                        <input class="form-control" type="number" name="price_cost">
                    </div>
                </div>
                <div class="text-right">
                    <button type="submit" class="btn btn-primary">Submit form <i
                            class="icon-arrow-right14 position-right"></i></button>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection
