@extends('layouts.limitless.index')
@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        <h5 class="panel-title">ระบบจัดการผู้ส่ง <a class="heading-elements-toggle"><i class="icon-more"></i></a>
        </h5>
        <div class="heading-elements">
        </div>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-bordered table-xxs" id="test-datatable">
                <thead>
                    <tr class="bg-grey-600">
                        <th>Sender ID</th>
                        <th>Sender Name</th>
                        <th>Sender Description</th>
                        <th class="text-center">Actions</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif
<script>
    $(function() {
         // Table setup
        // ------------------------------

        // Setting datatable defaults
        $.extend( $.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                searchPlaceholder: 'Type to filter...',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
            },
        });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    // Warning alert
        function deleteRecord(id) {
            confirmDelete(id);
        }
        $('.deleteRecord').on('click', function() {
            var id = $(this).attr('rel');
            confirmDelete(id);
        });

        $('#test-datatable').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ url('backend/senders/data')}}",
        "columns": [
            {"data":'id'},
            {"data":'sender_name'},
            {"data": null},
            {"data": null}
        ],
        buttons: [
            {
                text: 'สร้างผู้ส่งใหม่',
                className: 'btn btn-success heading-btn',
                action: function(e, dt, node, config) {
                    location.href = "{{ route('senders.create') }}";

                }
            }
        ],
        columnDefs: [
                {className: "text-center", "targets": [0, 1,3 ]},
                {className: "text-left", "targets": [2]},
                {
                    "render": function (data, type, row, meta) {
                        dataReturn = '<div style="white-space:pre-wrap;">'+data.description+'</div>';


                        return dataReturn;
                    },
                    "targets": 2
                },
                {
                    "render": function (data, type, row, meta) {
                        dataReturn = '';
                        dataReturn += '<a href="{{ url('backend/senders')}}/' + data.id + '/edit" class="label bg-green"><i class="icon-pencil7 position-left"></i>Edit</a>&nbsp;';
                        dataReturn += '<a class="label label-danger deleteRecord" onclick="deleteRecord('+ data.id +')" href="javascript:" rel="'+ data.id +'">Delete</a>';

                        return dataReturn;
                    },
                    "targets": 3
                },
            ],
    });

    });

    function deleteRecord(id) {
            confirmDelete(id);
    }

    function confirmDelete(id) {
            swal({
                title: "Confirm Delete",
                text: "Are you sure you want to delete this Senders?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }, function (isConfirm) {
                if (!isConfirm) return;
                $.ajax({
                    url : "{{ url('backend/senders')}}" + '/' + id,
                    type : "POST",
                    data : {'_method' : 'DELETE'},
                    success: function () {
                        swal({title: "Success", text: "Order has been deleted! \nClick 'Ok' to refresh page.", type: "success"},
                            function(){
                                location.reload();
                            }
                        );
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        swal("Error!", "Please try again", "error");
                    }
                });
            });
    }
</script>

@endsection
