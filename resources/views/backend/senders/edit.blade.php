@extends('layouts.limitless.index')
@section('content')

<div class="row">
    <div class="col-md-6">
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        {!! Form::model($sender, ['method' => 'PATCH','class'=>'form-horizontal','route' => ['senders.update',
        $sender->id]]) !!}

        <div class="panel panel-default">
            <div class="panel-heading">
                <h5 class="panel-title">แก้ไขใบผู้ส่ง<a class="heading-elements-toggle"><i class="icon-more"></i></a>
                </h5>
                <div class="heading-elements">
                    <a class="btn btn-primary" href="{{ route('senders.index') }}"> Back</a>
                </div>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-lg-3 control-label">ชื่อผู้ส่ง:<span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        {!! Form::text('sender_name', null, array('placeholder' => 'input validation','class' =>
                        'form-control','required'=>'required')) !!}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label">รายละเอียดผู้ส่ง:<span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <textarea rows="2" cols="5" name="description" id="description" class="form-control" required="required"
                            placeholder="Default textarea" aria-required="true" aria-invalid="true">{{ $sender->description }}</textarea>
                    </div>
                </div>
                <div class="text-right">
                    <button type="submit" class="btn btn-primary">Submit form <i
                            class="icon-arrow-right14 position-right"></i></button>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection
