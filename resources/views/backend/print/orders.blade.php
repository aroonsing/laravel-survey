@extends('layouts.limitless.index')
@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        <h5 class="panel-title">พิมพ์ใบสั่งซื้อ<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
        <div class="heading-elements">
            <a class="btn btn-primary heading-btn" href="JavaScript:void(0);" id="printSelected"><i
                    class="icon-printer position-left"></i>Print Selected</a>
        </div>
    </div>
    <div class="panel-body">
        <div class="row input-daterange">

            <div class="col-xs-1">
                เลือกวันทำการ
            </div>
            <div class="col-md-2">
                <div class="input-group">
                    <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                    <input type="text" name="from_date" id="from_date" class="form-control" placeholder="ตั้งแต่วันที่"
                        readonly />
                </div>

            </div>
            <div class="col-md-2">
                <div class="input-group">
                    <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                    <input type="text" name="to_date" id="to_date" class="form-control" placeholder="ถึงวันที่"
                        readonly />
                </div>

            </div>
            <div class="col-md-4">
                <button type="button" name="filter" id="filter" class="btn btn-primary">Filter</button>
                <button type="button" name="refresh" id="refresh" class="btn btn-default">Refresh</button>
            </div>
        </div>
        <br />
        {!! Form::open(array('route' => 'print-selected-orders','method'=>'POST','class'=>'form-horizontal myForm')) !!}
        <div class="table-responsive">
            <table class="table table-bordered table-xxs" id="test-datatable">
                <thead>
                    <tr class="bg-grey-600">
                        <th class="col-xs-1"><input type="checkbox" name="select_all" class="selectAll">&nbsp;All</th>
                        <th>ORDER ID</th>
                        <th>วันทำการ</th>
                        <th>DATE</th>
                        <th>TIME</th>
                        <th>ผู้สั่งซื้อ</th>
                        <th>เบอร์โทร</th>
                        <th>ที่อยู่</th>
                        <th>ตำบล อำเภอ จังหวัด</th>
                        <th>รหัสไปรษณี</th>
                        <th>สินค้า</th>
                        <th>วิธีชำระ</th>
                        <th>ยอดเงินรวม</th>
                        <th>ช่องทางการสั่งซื้อ</th>
                        <th>โปรไฟล์</th>
                        <th class="text-center">Actions</th>
                    </tr>
                </thead>
            </table>
        </div>
        {!! Form::close() !!}
    </div>
</div>
@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif
<script>
    $(function() {
        // Setting datatable defaults
        $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

        $('.input-daterange').datepicker({
            todayBtn:'linked',
            format:'yyyy-mm-dd',
            autoclose:true
        });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        load_data();
        function load_data(from_date = '', to_date = '')
        {
            $('#test-datatable').DataTable({
            processing: true,
            serverSide: true,
            "pageLength": 100,
            ajax: {
                url:'{{ url("/backend/printorders/data")}}',
                data:{from_date:from_date, to_date:to_date}
            },
            buttons: [
                {
                    extend: 'colvis',
                    className: 'btn btn-default',
                    columns: ':gt(0)'
                }

            ],
            columns: [
                {"data": null ,orderable: false, searchable: false},
                {"data":'id'},
                {"data":'working_date'},
                {"data":'order_date',searchable: false},
                {"data":'order_time',searchable: false},
                {"data":'name_surename'},
                {"data":'tel'},
                {"data":'address'},
                {"data": 'districts'},
                {"data":'zipcode'},
                {"data": 'product'},
                {"data":'payment'},
                {"data": 'total_price', render: $.fn.dataTable.render.number( ',', '.', 0 ) },
                {"data": 'purchase_channel'},
                {"data": 'profile'},
                {"data": null ,orderable: false, searchable: false}
            ],
            order: [[ 10, "desc", ],[1,"asc"]],
            columnDefs: [
                    // { "orderable": false, "targets": 13},
                    {className: "text-center", "targets": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 14]},
                    {className: "text-right", "targets": [12]},
                    {
                        "render": function (data, type, row, meta) {
                            dataReturn = '';
                            dataReturn += '<input type="checkbox" class="styled checkitem" name="checkitem[]" value="' + data.id +'">';
                            return dataReturn;
                        },
                        "targets": 0
                    },
                    {
                        "render": function (data, type, row, meta) {
                            dataReturn = '';
                            if(data.status_print == 0){
                                dataReturn += '<a href="{{ url('/backend/pdf/print-per-orders')}}/' + data.id +'" class="label bg-success"><i class="icon-printer position-left"></i>Print</a>';
                            }else{
                                dataReturn += '<a href="{{ url('/backend/pdf/print-per-orders')}}/' + data.id +'" class="label bg-danger"><i class="icon-printer position-left"></i>ปริ้นแล้ว</a>';
                            }

                            return dataReturn;
                        },
                        "targets": 15
                    },
                ],
            });
        }

        $('#filter').click(function(){
        var from_date = $('#from_date').val();
        var to_date = $('#to_date').val();
        if(from_date != '' &&  to_date != '')
        {
        $('#test-datatable').DataTable().destroy();
        load_data(from_date, to_date);
        }
        else
        {
        alert('กรุณาเลือกวันทำการ');
        }
        });

        $('#refresh').click(function(){
            $('#from_date').val('');
            $('#to_date').val('');
            $('#test-datatable').DataTable().destroy();
            load_data();
        });



            $('.selectAll').on('click', function(e) {
                if($(this).is(':checked',true))
                {
                    $(".checkitem").prop('checked', true);
                }
                else
                {
                    $(".checkitem").prop('checked',false);
                }
            });

        $('#printSelected').click(function () {
            var id = $('.checkitem:checked').map(function () {
                return $(this).val();
            }).get().join(',');
            if(id == ''){
                swal("Error!", "กรุณาเลือก", "error");
            }else{
                $(".myForm").submit();
            }


        });
    });

</script>

@endsection
