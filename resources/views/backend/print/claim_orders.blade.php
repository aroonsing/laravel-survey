@extends('layouts.limitless.index')
@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        <h5 class="panel-title">พิมพ์ใบเคลม <a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
        <div class="heading-elements">
            <a class="btn btn-primary heading-btn" href="JavaScript:void(0);" id="printSelected"><i
                    class="icon-printer position-left"></i>Print Selected</a>
        </div>
    </div>
    <div class="panel-body">
        {!! Form::open(array('route' => 'print-selected-claimorders','method'=>'POST','class'=>'form-horizontal
        myForm')) !!}
        <div class="table-responsive">
            <table class="table table-bordered table-xxs" id="test-datatable">
                <thead>
                    <tr class="bg-grey-600">
                        <th class="col-xs-1"><input type="checkbox" name="select_all" class="selectAll">&nbsp;All</th>
                        <th>Claim ID</th>
                        <th>DATE</th>
                        <th>TIME</th>
                        <th>หัวข้อเคลม</th>
                        <th>รายละเอียดการเคลม</th>
                        <th>ชื่อผู้เคลม</th>
                        <th>เบอร์โทร</th>
                        <th>ที่อยู่</th>
                        <th>ตำบล อำเภอ จังหวัด</th>
                        <th>รหัสไปรษณี</th>
                        <th>ยอดเงินรวม</th>
                        <th class="text-center">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {{-- @foreach ($data as $key => $claim_order)
                    <tr>
                        <td class="text-center">{{$claim_order->id}}</td>
                    <td class="text-center">{{$claim_order->order_date}}</td>
                    <td class="text-center">{{$claim_order->order_time}}</td>
                    <td class="text-center">{{$claim_order->claim_name}}</td>
                    <td class="text-center">{{$claim_order->description}}</td>
                    <td class="text-center">{{$claim_order->name_surename}}</td>
                    <td class="text-center">{{$claim_order->tel}}</td>
                    <td class="text-center">{{$claim_order->address}}</td>
                    <td class="text-center">{{$claim_order->districts}}</td>
                    <td class="text-center">{{$claim_order->zip_code}}</td>
                    <td class="text-center">{{$claim_order->total_price}}</td>
                    <td>
                        <a href="{{ url('/backend/pdf/printclaim-per-orders/'.$claim_order->id) }}"
                            class="label bg-primary"><i class="icon-printer position-left"></i>Print</a>
                    </td>
                    </tr>
                    @endforeach --}}
                </tbody>
            </table>
        </div>
        {!! Form::close() !!}
    </div>
    @if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
    @endif
    <script>
        $(function() {
            // Setting datatable defaults
            $.extend( $.fn.dataTable.defaults, {
                autoWidth: false,
                dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Filter:</span> _INPUT_',
                    searchPlaceholder: 'Type to filter...',
                    lengthMenu: '<span>Show:</span> _MENU_',
                    paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
                },
                drawCallback: function () {
                    $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
                },
                preDrawCallback: function() {
                    $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
                }
            });

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#test-datatable').DataTable({
                processing: true,
                serverSide: true,
                "pageLength": 100,
                ajax: "{{ url('/backend/printclaimorders/data')}}",
                columns: [
                    {"data": null ,orderable: false, searchable: false},
                    {"data":'id'},
                    {"data":'order_date'},
                    {"data":'order_time'},
                    {"data":'claim_name'},
                    {"data":'description'},
                    {"data":'name_surename'},
                    {"data": 'tel'},
                    {"data":'address'},
                    {"data": 'districts'},
                    {"data":'zipcode'},
                    {"data": 'total_price', render: $.fn.dataTable.render.number( ',', '.', 0 ) },
                    {"data": null ,orderable: false, searchable: false}
                ],
                order: [[ 9, "desc", ],[1,"asc"]],
                columnDefs: [
                        // { "orderable": false, "targets": 13},
                        {className: "text-center", "targets": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12]},
                        {className: "text-right", "targets": [11]},
                        {
                            "render": function (data, type, row, meta) {
                                dataReturn = '';
                                dataReturn += '<input type="checkbox" class="styled checkitem" name="checkitem[]" value="' + data.id +'">';
                                return dataReturn;
                            },
                            "targets": 0
                        },
                        {
                            "render": function (data, type, row, meta) {
                                dataReturn = '';
                                if(data.status_print == 0){
                                    dataReturn += '<a href="{{ url('/backend/pdf/printclaim-per-orders')}}/' + data.id +'" class="label bg-success"><i class="icon-printer position-left"></i>Print</a>';
                                }else{
                                    dataReturn += '<a href="{{ url('/backend/pdf/printclaim-per-orders')}}/' + data.id +'" class="label bg-danger"><i class="icon-printer position-left"></i>ปริ้นแล้ว</a>';
                                }

                                return dataReturn;
                            },
                            "targets": 12
                        },
                    ],
                });

                $('.selectAll').on('click', function(e) {
                    if($(this).is(':checked',true))
                    {
                        $(".checkitem").prop('checked', true);
                    }
                    else
                    {
                        $(".checkitem").prop('checked',false);
                    }
                });

            $('#printSelected').click(function () {
                var id = $('.checkitem:checked').map(function () {
                    return $(this).val();
                }).get().join(',');
                if(id == ''){
                    swal("Error!", "กรุณาเลือก", "error");
                }else{
                    $(".myForm").submit();
                }


            });
        });

    </script>
    @endsection
