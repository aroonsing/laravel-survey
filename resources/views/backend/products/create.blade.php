@extends('layouts.limitless.index')
@section('content')

<div class="row">
    <div class="col-md-6">
            @if (count($errors) > 0)
            <div class="alert alert-danger">
              <strong>Whoops!</strong> There were some problems with your input.<br><br>
              <ul>
                 @foreach ($errors->all() as $error)
                   <li>{{ $error }}</li>
                 @endforeach
              </ul>
            </div>
            @endif
            <form class="form-horizontal form-validate-jquery" method="POST" action="{{ route('products.store') }}" enctype="multipart/form-data">
                @csrf
                <div class="panel panel-default">
                <div class="panel-heading">
                    <h5 class="panel-title">สร้างสินค้า<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
                    <div class="heading-elements">
                            <a class="btn btn-primary" href="{{ route('products.index') }}"> Back</a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-lg-3 control-label">ชื่อสินค้า:</label>
                        <div class="col-lg-9">
                            {!! Form::text('product_name', null, array('placeholder' => 'Product Name input validation','class' => 'form-control')) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label">รหัสสินค้า:</label>
                        <div class="col-lg-9">
                            {!! Form::text('product_code', null, array('placeholder' => 'Product code input validation','class' => 'form-control')) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label">รายละเอียดสินค้า:</label>
                        <div class="col-lg-9">
                            {!! Form::textarea('description', null, array('placeholder' => 'Description input validation','class' => 'form-control')) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Image <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="file" name="image" id="image" class="form-control" required="required"
                                placeholder="image input validation">
                        </div>
                    </div>
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary">Submit form <i class="icon-arrow-right14 position-right"></i></button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
