@extends('layouts.limitless.index')

@section('content')
<div class="row">
    <div class="col-md-6">
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        {!! Form::model($product, ['method' => 'PATCH','class'=>'form-horizontal','files' => true,'route' =>
        ['products.update', $product->id]]) !!}

        <div class="panel panel-default">
            <div class="panel-heading">
                <h5 class="panel-title">แก้ไขสินค้า<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
                <div class="heading-elements">
                    <a class="btn btn-primary" href="{{ route('products.index') }}"> Back</a>
                </div>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-lg-3 control-label">ชื่อสินค้า:</label>
                    <div class="col-lg-9">
                        {!! Form::text('product_name', null, array('placeholder' => 'Product Name input
                        validation','class' => 'form-control')) !!}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label">รหัสสินค้า:</label>
                    <div class="col-lg-9">
                        {!! Form::text('product_code', null, array('placeholder' => 'Product code input
                        validation','class' => 'form-control')) !!}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label">รายละเอียดสินค้า:</label>
                    <div class="col-lg-9">
                        {!! Form::textarea('description', null, array('placeholder' => 'Description input
                        validation','class' => 'form-control')) !!}
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-3">Image <span class="text-danger">*</span></label>
                    <div class="col-lg-6">
                        <input type="file" name="image" id="image" class="form-control"
                            placeholder="image input validation">
                        <input value="{{ $product->image}}" type="hidden" name="current_image">

                    </div>
                    <div class="col-lg-3">
                        @empty(!$product->image)
                        <img src="{{ asset('/images/backend/products/small/'.$product->image)}}" width="40px">
                        <a href="{{ url('/backend/products/delete-image/'.$product->id) }}"><i
                                class="icon-trash"></i></a>
                        @endempty

                    </div>
                </div>
                <div class="text-right">
                    <button type="submit" class="btn btn-primary">Submit form <i
                            class="icon-arrow-right14 position-right"></i></button>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>
@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif
@endsection
