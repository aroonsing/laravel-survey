@extends('layouts.limitless.index')

@section('extra-scripts')
<!-- Theme JS files -->
<script type="text/javascript" src="{{ asset('assets_backend/js/plugins/forms/validation/validate.min.js')}}">
</script>
<script type="text/javascript" src="{{ asset('assets_backend/js/plugins/forms/selects/bootstrap_multiselect.js')}}">
</script>
<script type="text/javascript" src="{{ asset('assets_backend/js/plugins/forms/inputs/touchspin.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets_backend/js/plugins/forms/selects/select2.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets_backend/js/plugins/forms/styling/switch.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets_backend/js/plugins/forms/styling/switchery.min.js')}}">
</script>
<script type="text/javascript" src="{{ asset('assets_backend/js/plugins/forms/styling/uniform.min.js')}}"></script>

<script type="text/javascript" src="{{ asset('assets_backend/js/pages/form_validation.js')}}"></script>
<!-- /theme JS files -->
@endsection

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        <h5 class="panel-title">Add Product Attributes<a class="heading-elements-toggle"><i class="icon-more"></i></a>
        </h5>
        <div class="heading-elements">
            <a class="btn btn-primary" href="{{ route('products.index') }}"> Back</a>
        </div>
    </div>

    <div class="panel-body">
        @if(Session::has('flash_message_error'))
        <script>
            $(function() {
                new PNotify({
                    title: 'Danger notice',
                    text: '{!! session('flash_message_error') !!}',
                    icon: 'icon-blocked',
                    type: 'error'
                });
            });
        </script>
        @endif
        @if(Session::has('flash_message_success'))
        <script>
            $(function() {
            // Success notification
                new PNotify({
                    title: 'Success notice',
                    text: '{!! session('flash_message_success') !!}',
                    icon: 'icon-checkmark3',
                    type: 'success'
                });
            });
        </script>
        @endif


        <form class="form-horizontal form-validate-jquery" method="POST"
            action="{{ url('/backend/products/add-attributes/'.$productDetails->id) }}" enctype="multipart/form-data">
            @csrf
            <fieldset class="content-group">
                <input type="hidden" name="product_id" value="{{$productDetails->id}}">
                <div class="form-group">
                    <label class="control-label col-xs-3">Product Name <span class="text-danger">*</span></label>
                    <div class="col-xs-9">
                        <div class="form-control-static">{{$productDetails->product_name}}</div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-xs-3">Product code <span class="text-danger">*</span></label>
                    <div class="col-xs-9">
                        <div class="form-control-static">{{$productDetails->product_code}}</div>
                    </div>
                </div>
                <div class="field_wrapper">
                    <div class="form-group">
                        <label class="control-label col-lg-3"></label>
                        <div class="col-lg-9">
                            <div class="row">
                                <div class="col-md-2"><input name="sku[]" id="sku" placeholder="SKU" type="text"
                                        class="form-control" required /></div>
                                <div class="col-md-2"><input name="color[]" id="color" placeholder="สี" type="text"
                                        class="form-control" required /></div>
                                <div class="col-md-2">
                                    <span class="label bg-primary-800 help-inline add_button" style="cursor: pointer;" title="Add field">เพิ่มแถว</span>
                                </div>
                                {{-- <div class="col-md-3"><a href="javascript:void(0);" class="add_button"
                                        title="Add field"><i class="glyphicon glyphicon-plus"></i></a></div> --}}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="text-right">
                    <button type="reset" class="btn btn-default" id="reset">Reset <i
                            class="icon-reload-alt position-right"></i></button>
                    <button type="submit" class="btn btn-primary">Submit <i
                            class="icon-arrow-right14 position-right"></i></button>
                </div>
            </fieldset>
        </form>
    </div>
    <table class="table datatable-html">
        <thead>
            <tr>
                <th>Attribute ID</th>
                <th>SKU</th>
                <th>สี</th>
                <th>Stock</th>
                <th class="text-center">Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($productDetails['attributes'] as $attribute)
            <tr>
                <td class="text-center">{{$attribute->id}}</td>
                <td class="text-center">{{$attribute->sku}}</td>
                <td>{{$attribute->color}}</td>
                <td>{{$attribute->stock}}</td>
                <td class="">
                    <a class="label label-danger deleteRecord" href="javascript:" rel="{{$attribute->id}}"
                        rel1="backend/products/delete-attribute">Delete</a>
                </td>
            </tr>
            @endforeach



        </tbody>
    </table>
</div>
<script>
    $(function() {
        // Table setup
        // ------------------------------

        // Setting datatable defaults
        $.extend( $.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                searchPlaceholder: 'Type to filter...',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
            },
            drawCallback: function () {
                $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
            },
            preDrawCallback: function() {
                $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
            }
        });
    // HTML sourced data
    $('.datatable-html').dataTable();
    var validator = $(".form-validate-jquery").validate({
            ignore: 'input[type=hidden]', // ignore hidden fields
            errorClass: 'validation-error-label',
            successClass: 'validation-valid-label',
            highlight: function(element, errorClass) {
                $(element).removeClass(errorClass);
            },
            unhighlight: function(element, errorClass) {
                $(element).removeClass(errorClass);
            },
            validClass: "validation-valid-label",
            success: function(label) {
                label.addClass("validation-valid-label").text("Success.")
            },
            // rules: {
            //     image: {
            //        required;
            //     },
            // },
            messages: {
                custom: {
                    required: "This is a custom error message",
                },
                agree: "Please accept our policy"
            }
        });

         // Reset form
         $('#reset').on('click', function() {
            validator.resetForm();
        });

        // Warning alert
        $('.deleteRecord').on('click', function() {
            var id = $(this).attr('rel');
            var deleteFunction = $(this).attr('rel1');
            swal({
                title: "Are you sure?",
                text: "You will not be able to revert this!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#FF7043",
                confirmButtonText: "Yes, delete it!",
                buttonStyling:false,
                reverseButtons: true
            },function(){
                // alert('test');
                window.location.href = "/"+deleteFunction+"/"+id;
            });
        });
});
</script>
<script type="text/javascript">
    $(document).ready(function(){
        var maxField = 10; //Input fields increment limitation
        var addButton = $('.add_button'); //Add button selector
        var wrapper = $('.field_wrapper'); //Input field wrapper
        var fieldHTML = '<div class="form-group">'+
                        '<label class="control-label col-lg-3"></label>'+
                        '    <div class="col-lg-9">'+
                        '        <div class="row">'+
                        '            <div class="col-md-2"><input name="sku[]" id="sku" placeholder="SKU" type="text" class="form-control"></div>'+
                        '            <div class="col-md-2"><input name="color[]" id="color" placeholder="สี" type="text" class="form-control"></div>'+
                        '            <div class="col-md-2"><span class="label bg-danger help-inline remove_button" style="cursor: pointer;" title="Delete field">ลบแถว</span>'+
                        '        </div>'+
                        '    </div>'+
                        '</div>';
        // var fieldHTML = '<div><input type="text" name="field_name[]" value=""/><a href="javascript:void(0);" class="remove_button"><i class="glyphicon glyphicon-minus"/></i></div>'; //New input field html
        var x = 1; //Initial field counter is 1

        //Once add button is clicked
        $(addButton).click(function(){
            //Check maximum number of input fields
            if(x < maxField){
                x++; //Increment field counter
                $(wrapper).append(fieldHTML); //Add field html
            }
        });

        //Once remove button is clicked
        $(wrapper).on('click', '.remove_button', function(e){
            e.preventDefault();
            $(this).parent('div').parent('div').parent('div').parent('div').remove(); //Remove field html
            x--; //Decrement field counter
        });
    });
</script>
@endsection
