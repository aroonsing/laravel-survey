@extends('layouts.limitless.index')
@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        <h5 class="panel-title">ระบบจัดการสินค้า <a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
        <div class="heading-elements">
            {{-- @can('user-create') --}}
            <a class="btn btn-success heading-btn" href="{{ route('products.create') }}">สร้างสินค้าใหม่</a>
            {{-- @endcan --}}
        </div>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-bordered table-xxs" id="test-datatable">
                <thead>
                    <tr class="bg-grey-600">
                        <th style="width: 50px;"></th>
                        <th class="col-xs-1">Product ID</th>
                        <th class="col-xs-1">Image</th>
                        <th class="col-xs-2">Product Name</th>
                        <th>Product Code</th>
                        <th class="col-xs-1 text-center">Actions</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif
<script id="details-template" type="text/x-handlebars-template">
    @verbatim
    <table class="table table-bordered table-xxs" id="attributes-{{id}}">
        <thead>
            <tr class="bg-grey-300">
                <th class="col-xs-1">Attribute ID</th>
                <th class="col-xs-2">Attribute SKU</th>
                <th>AttributeColor</th>
                <th>จำนวนคงเหลือใน Stock</th>
                <th class=" col-xs-2 text-center">Actions</th>
            </tr>
        </thead>
    </table>
    @endverbatim
</script>
<script>
    $(function() {
        // Setting datatable defaults
        $.extend( $.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                searchPlaceholder: 'Type to filter...',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
            },
            drawCallback: function () {
                $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
            },
            preDrawCallback: function() {
                $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
            }
        });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var template = Handlebars.compile($("#details-template").html());

        var table = $('#test-datatable').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ url('backend/products/data')}}",
            columns: [
                {
                    "class":          "details-control",
                    "orderable":      false,
                    "data":           null,
                    "searchable":     false,
                    "defaultContent": ""
                },
                {"data":'id'},
                {"data": null ,orderable: false, searchable: false},
                {"data":'product_name'},
                {"data": 'product_code'},
                {"data": null ,orderable: false, searchable: false}
            ],
            order: [[ 1, "desc" ]],
            columnDefs: [
                    // { "orderable": false, "targets": 13},
                    {className: "text-center", "targets": [0, 1, 2, 3, 4, 5]},
                    {
                        "render": function (data, type, row, meta) {

                            dataReturn = '<img src="{{ asset('/images/backend/products/small')}}/'+ data.image + '" width="60">';
                            return dataReturn;
                        },
                        "targets": 2
                    },
                    {
                        "render": function (data, type, row, meta) {
                            dataReturn = '';
                            dataReturn += '<ul class="icons-list">';
                            dataReturn += '	<li class="dropdown">';
                            dataReturn += '		<a href="#" class="dropdown-toggle" data-toggle="dropdown">';
                            dataReturn += '			<i class="icon-menu9"></i>';
                            dataReturn += '		</a>';
                            dataReturn += '		<ul class="dropdown-menu dropdown-menu-right">';
                            dataReturn += '			<li><a href="{{url('backend/products')}}/' + data.id + '/edit"><i class="icon-pencil7"></i>แก้ไข</a></li>';
                            dataReturn += '			<li><a href="{{url('/backend/products/add-attributes')}}/' + data.id + '"><i class="icon-forward"></i>จัดการสีสินค้า</a></li>';
                            dataReturn += '			<li><a href="javascript:" onclick="deleteRecord(' + data.id + ')" rel=""><i class="icon-bin"></i>ลบ</a></li>';

                            dataReturn += '		</ul>';
                            dataReturn += '	</li>';
                            dataReturn += '</ul>';

                            return dataReturn;
                        },
                        "targets": 5
                    },
                ],
            });
            // Add event listener for opening and closing details
        $('#test-datatable tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            console.log(tr);
            var row = table.row(tr);

            var tableId = 'attributes-' + row.data().id;

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            } else {
                // Open this row
                row.child(template(row.data())).show();
                initTable(tableId, row.data());
                tr.addClass('shown');
                tr.next().find('td').addClass('no-padding bg-gray');
            }
        });
    });



    function initTable(tableId, data) {
        $('#' + tableId).DataTable({
            processing: true,
            lengthChange: false,
            searching:false,
            serverSide: true,
            ajax: data.details_url,
            columns: [
                { data: 'id', name: 'id' },
                { data: 'sku', name: 'sku' },
                { data: 'color', name: 'color' },
                { data: 'stock', name: 'stock' },
                { data: null , orderable: false, searchable: false}
            ],
            order: [[ 1, "asc" ]],
            columnDefs: [
                    // { "orderable": false, "targets": 13},
                    {className: "text-center", "targets": [0, 1, 2, 4]},
                    {className: "text-right", "targets": [3]},

                    {
                        "render": function (data, type, row, meta) {
                            dataReturn = '';
                            dataReturn += '<ul class="icons-list">';
                            dataReturn += '	<li class="dropdown">';
                            dataReturn += '		<a href="#" class="dropdown-toggle" data-toggle="dropdown">';
                            dataReturn += '			<i class="icon-menu9"></i>';
                            dataReturn += '		</a>';
                            dataReturn += '		<ul class="dropdown-menu dropdown-menu-right">';
                            dataReturn += '         <li><a href="{{url('backend/stocks')}}/' + data.id + '/edit"><i class="icon-plus2"></i>เพิ่ม Stock</a></li>';
                            dataReturn += '         <li><a href="{{url('backend/stocks/manage')}}/' + data.id + '"><i class="icon-pencil7 position-left"></i>จัดการ Stock</a></li>';
                            dataReturn += '		</ul>';
                            dataReturn += '	</li>';
                            dataReturn += '</ul>';

                            return dataReturn;
                        },
                        "targets": 4
                    },
                ],
        })
    }

    function deleteRecord(id) {
            swal({
                title: "Confirm Delete",
                text: "Are you sure you want to delete this Product ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }, function (isConfirm) {
                if (!isConfirm) return;
                $.ajax({
                    url : "{{ url('backend/products')}}" + '/' + id,
                    type : "POST",
                    data : {'_method' : 'DELETE'},
                    success: function () {
                        swal({title: "Success", text: "Product has been deleted! \nClick 'Ok' to refresh page.", type: "success"},
                            function(){
                                location.reload();
                            }
                        );
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        swal("Error!", "Please try again", "error");
                    }
                });
            });
    }
</script>

@endsection
