@extends('layouts.limitless.index')
@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        <h5 class="panel-title">รายงานใบเคลม <a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
        <div class="heading-elements">
        </div>
    </div>
    <div class="panel-body">

        <div class="row input-daterange">
            <div class="col-md-2">
                <div class="input-group">
                    <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                    <input type="text" name="from_date" id="from_date" class="form-control" placeholder="From Date"
                        readonly />
                </div>

            </div>
            <div class="col-md-2">
                <div class="input-group">
                    <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                    <input type="text" name="to_date" id="to_date" class="form-control" placeholder="To Date"
                        readonly />
                </div>

            </div>
            <div class="col-md-4">
                <button type="button" name="filter" id="filter" class="btn btn-primary">Filter</button>
                <button type="button" name="refresh" id="refresh" class="btn btn-default">Refresh</button>
            </div>
        </div>
        <br />
        <div class="table-responsive">
            <table class="table table-bordered table-xxs" id="test-datatable">
                <thead>
                    <tr class="bg-grey-600">
                        <th>Claim ID</th>
                        <th>DATE</th>
                        <th>TIME</th>
                        <th>หัวข้อเคลม</th>
                        <th>รายละเอียดการเคลม</th>
                        <th>ชื่อผู้เคลม</th>
                        <th>เบอร์โทร</th>
                        <th>ที่อยู่</th>
                        <th>ตำบล อำเภอ จังหวัด</th>
                        <th>รหัสไปรษณี</th>
                        <th>ยอดเงินรวม</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif
<script>
    $(document).ready(function(){

        // Setting datatable defaults
        $.extend( $.fn.dataTable.defaults, {
                autoWidth: false,
                dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Filter:</span> _INPUT_',
                    searchPlaceholder: 'Type to filter...',
                    lengthMenu: '<span>Show:</span> _MENU_',
                    paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
                }
            });

     $('.input-daterange').datepicker({
      todayBtn:'linked',
      format:'yyyy-mm-dd',
      autoclose:true
     });

     load_data();

     function load_data(from_date = '', to_date = '')
     {
      $('#test-datatable').DataTable({
       processing: true,
       serverSide: true,
       ajax: {
        url:'{{ route("report-claimorders.indexData") }}',
        data:{from_date:from_date, to_date:to_date}
       },
       "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
       "columns": [
            {"data":'id'},
            {"data":'order_date'},
            {"data":'order_time'},
            {"data":'claim_name'},
            {"data":'description'},
            {"data":'name_surename'},
            {"data": 'tel'},
            {"data":'address'},
            {"data": 'districts'},
            {"data":'zip_code'},
            {"data": 'total_price', render: $.fn.dataTable.render.number( ',', '.', 0 ) }

        ],
        columnDefs: [
                {className: "text-center", "targets": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9,]},
                {className: "text-right", "targets": [10]},
            ],
            buttons: {
            dom: {
                button: {
                    className: 'btn btn-default'
                }
            },
            buttons: [
                {
                    extend: 'excelHtml5',
                    text : 'Export to Excel <i class="icon-file-pdf position-right"></i>',
                    filename: function(){
                        var d = new Date();
                        var n = d.getTime();
                        return 'ReportClaim' + n;
                    },
                }
                // 'copyHtml5',
                // 'excelHtml5',
                // 'csvHtml5',
                // 'pdfHtml5'
            ]
        },
      });
     }

     $('#filter').click(function(){
      var from_date = $('#from_date').val();
      var to_date = $('#to_date').val();
      if(from_date != '' &&  to_date != '')
      {
       $('#test-datatable').DataTable().destroy();
       load_data(from_date, to_date);
      }
      else
      {
       alert('Both Date is required');
      }
     });

      $('#refresh').click(function(){
      $('#from_date').val('');
      $('#to_date').val('');
      $('#test-datatable').DataTable().destroy();
      load_data();
     });

    });
</script>
@endsection
