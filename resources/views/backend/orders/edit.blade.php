@extends('layouts.limitless.index')
@section('content')

<div class="row">
    <div class="col-md-6">
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        {!! Form::model($order, ['method' => 'PATCH','class'=>'form-horizontal','route' => ['orders.update',
        $order->id]]) !!}

        <div class="panel panel-default">
            <div class="panel-heading">
                <h5 class="panel-title">แก้ไขใบสั่งซื้อ<a class="heading-elements-toggle"><i class="icon-more"></i></a>
                </h5>
                <div class="heading-elements">
                    <a class="btn btn-primary" href="{{ route('orders.index') }}"> Back</a>
                </div>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-lg-3 control-label">เลือกผู้ส่ง:<span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <select name="sender_id" id="sender_id"  class="form-control"
                            required="required">
                            <option value="">เลือก</option>
                            @foreach ($senders as $sender)
                            <option value="{{$sender->id}}" @if ($sender->id == $order->sender_id) echo selected
                                @endif>{{$sender->sender_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label">ชื่อ-นามสกุล:<span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        {!! Form::text('name_surename', null, array('placeholder' => 'input validation','class' =>
                        'form-control')) !!}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label">เบอร์โทรศัพท์<span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        {!! Form::text('tel', null, array('placeholder' => 'input validation','class' =>
                        'form-control')) !!}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label">ที่อยู่:<span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <textarea rows="2" cols="5" name="address" id="address" class="form-control" required="required"
                            placeholder="Default textarea" aria-required="true"
                            aria-invalid="true">{{$order->address}}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label">ตำบล:<span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <input type="text" name="districts" id="district" class="form-control" required="required"
                            value="{{$order->districts}}">

                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label">อำเภอ:<span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <input type="text" name="amphures" id="amphoe" class="form-control" required="required"
                            value="{{$order->amphures}}">

                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label">จังหวัด:<span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <input type="text" name="provinces" id="province" class="form-control" required="required"
                            value="{{$order->provinces}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-3">รหัสไปรษณี <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <input type="text" name="zipcode" id="zipcode" class="form-control" required="required"
                            value="{{$order->zipcode}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label">เลือกสินค้า:<span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <select name="product_id" id="product_id" onchange="showProductAtttributes()"
                            class="form-control" required="required">
                            <option value="">เลือก</option>
                            @foreach ($products as $product)
                            <option value="{{$product->id}}" @if ($product->id == $order->product_id) echo selected
                                @endif>{{$product->product_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label">เลือกรายละเอียดสินค้า:<span
                            class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <select name="products_attributes_id" id="products_attributes_id" class="form-control"
                            required="required">
                            <option value="">เลือก</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label">ประเภทการชำระเงิน:<span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <select name="payment" class="form-control" required="required">
                            <option value="0">เลือก</option>
                            <option value="โอน" @if ($order->payment == 'โอน') echo selected @endif>โอน</option>
                            <option value="COD" @if ($order->payment == 'COD') echo selected @endif>เก็บเงินปลายทาง
                            </option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-3">ยอดชำระ:<span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <input type="text" name="total_price" value="{{$order->total_price}}" id="" class="form-control"
                            required="required" placeholder="input validation">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-3">ช่องทางการสั่งซื้อ:<span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <select name="purchase_channel" class="form-control" required="required">
                            <option value="">เลือก</option>
                            <option @if ($order->purchase_channel == 'FACEBOOK') echo selected @endif
                                value="FACEBOOK">FACEBOOK</option>
                            <option @if ($order->purchase_channel == 'Line') echo selected @endif value="Line">Line
                            </option>
                            <option @if ($order->purchase_channel == 'Tel') echo selected @endif value="Tel">Tel
                            </option>
                            <option @if ($order->purchase_channel == 'Offline') echo selected @endif
                                value="Offline">Offline</option>
                            <option @if ($order->purchase_channel == 'Shopee') echo selected @endif
                                value="Shopee">Shopee</option>
                            <option @if ($order->purchase_channel == 'Lazada') echo selected @endif
                                value="Lazada">Lazada</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-3">ข้อมูล โปรไฟล์:<span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <input type="text" name="profile" value="{{$order->profile}}" id="" class="form-control"
                            required="required" placeholder="input validation">
                    </div>
                </div>
                <div class="text-right">
                    <button type="submit" class="btn btn-primary">Submit form <i
                            class="icon-arrow-right14 position-right"></i></button>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>
<script>
    $(document).ready(function(){
        // amphures_id = {{$order->amphures_id}};
        // districts_id = {{$order->districts_id}};
        products_attributes_id = {{$order->products_attributes_id}};
        // showProvinces();
        showProductAtttributes();
        $.Thailand({
            $district: $('#district'), // input ของตำบล
            $amphoe: $('#amphoe'), // input ของอำเภอ
            $province: $('#province'), // input ของจังหวัด
            $zipcode: $('#zipcode'), // input ของรหัสไปรษณีย์
        });
    });
    /*function showProvinces(){
         $.ajax({
                type:'get',
                url:'{{ url('/') }}/api/province',
                success:function(resp){
                    $("#input_province").empty();
                    $("#input_province").append(
                        $('<option></option>')
                        .attr("value", "")
                        .html("กรุณาเลือกจังหวัด")
                    );
                    for(var i=0; i<resp.length; i++){
                    $("#input_province").append(
                        $('<option></option>')
                        .attr("value", ""+resp[i].id)
                        .html(""+resp[i].name_th)
                    );
                    }
                    $("#input_province").val('{{$order->provinces_id}}');
                    showAmphoes();
                },error:function(){
                    alert("error");
                }
            });
    }*/

    /*function showAmphoes(){
        //INPUT
        var province_id = $("#input_province").val();
        if(province_id == ''){
            $("#input_amphoe").empty();
            $("#input_amphoe").append($('<option></option>').attr("value", "").html("กรุณาเลือกอำเภอ"));
            $("#input_district").empty();
            $("#input_district").append($('<option></option>').attr("value", "").html("กรุณาเลือกตำบล"));
            $("#zipcode").val('');
            return;
        }
        $.ajax({
            type:'get',
            url:"{{ url('/') }}/api/province/"+province_id+"/amphoe",
            success:function(resp){
                //console.log(resp);
                $("#input_amphoe").empty();
                $("#input_amphoe").append(
                        $('<option></option>')
                        .attr("value", "")
                        .html("กรุณาเลือกอำเภอ")
                    );
                for(var i=0; i<resp.length; i++){
                $("#input_amphoe").append(
                    $('<option></option>')
                    .attr("value", ""+resp[i].id)
                    .html(""+resp[i].name_th)
                );
                }
                if(amphures_id != ''){
                    $("#input_amphoe").val('{{$order->amphures_id}}');
                    amphures_id = '';
                }

                showDistricts();
            },error:function(){
                alert("error");

            }
        });
    }*/

    /*function showDistricts(){

     //INPUT
     var amphoe_id = $("#input_amphoe").val();
     if(amphoe_id == ''){
            $("#input_district").empty();
            $("#input_district").append($('<option></option>').attr("value", "").html("กรุณาเลือกตำบล"));
            $("#zipcode").val('');
            return;
        }
        $.ajax({
            type:'get',
            url:"{{ url('/') }}/api/amphoe/"+amphoe_id+"/district",
            success:function(resp){
                //console.log(result);
                $("#input_district").empty();
                $("#input_district").append(
                        $('<option></option>')
                        .attr("value", "")
                        .html("กรุณาเลือกตำบล")
                    );
                for(var i=0; i<resp.length; i++){
                $("#input_district").append(
                    $('<option></option>')
                    .attr("value", ""+resp[i].id)
                    .attr("data-zipcode", ""+resp[i].zip_code)
                    .html(""+resp[i].name_th)
                );
                }
                if(districts_id != ''){
                    $("#input_district").val('{{$order->districts_id}}');
                    districts_id = '';
                }
                showZipcode();
            },error:function(){
                alert("error");

            }
        });
    }*/

    /*function showZipcode(){
    //INPUT
    var zip_code = $("#input_district option:selected").attr('data-zipcode');
    $("#zipcode").val(zip_code);

    }*/

    function showProductAtttributes(){
         //INPUT
        var product_id = $("#product_id").val();
        if(product_id == ''){
            $("#products_attributes_id").empty();
            $("#products_attributes_id").append($('<option></option>').attr("value", "").html("เลือก"));
            return;
        }
        $.ajax({
            type:'get',
            url:"{{ url('/') }}/api/products_attributes/"+product_id,
            success:function(resp){
                //console.log(resp);
                $("#products_attributes_id").empty();
                for(var i=0; i<resp.length; i++){
                $("#products_attributes_id").append(
                    $('<option></option>')
                    .attr("value", ""+resp[i].id)
                    .html(""+resp[i].sku)
                );
                }
                if(products_attributes_id != ''){
                    console.log(products_attributes_id);
                    $("#products_attributes_id").val('{{$order->products_attributes_id}}');
                    products_attributes_id = '';
                }
            },error:function(){
                alert("error");

            }
        });
    }

</script>

@endsection
