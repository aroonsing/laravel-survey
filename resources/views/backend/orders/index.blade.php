@extends('layouts.limitless.index')
@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        <h5 class="panel-title">รายละเอียดใบสั่งซื้อ <a class="heading-elements-toggle"><i class="icon-more"></i></a>
        </h5>
        <div class="heading-elements">
        </div>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-bordered table-xxs" id="test-datatable">
                <thead>
                    <tr class="bg-grey-600">
                        <th>ORDER ID</th>
                        <th>DATE</th>
                        <th>TIME</th>
                        <th>ผู้สั่งซื้อ</th>
                        <th>เบอร์โทร</th>
                        <th>ที่อยู่</th>
                        <th>ตำบล อำเภอ จังหวัด</th>
                        <th>รหัสไปรษณี</th>
                        <th>สินค้า</th>
                        <th>วิธีชำระ</th>
                        <th>ยอดเงินรวม</th>
                        <th>ช่องทางการสั่งซื้อ</th>
                        <th>โปรไฟล์</th>
                        <th class="text-center">Actions</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif
<script>
    $(function() {
         // Table setup
        // ------------------------------

        // Setting datatable defaults
        $.extend( $.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                searchPlaceholder: 'Type to filter...',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
            },
            drawCallback: function () {
                $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
            },
            preDrawCallback: function() {
                $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
            }
        });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    // Warning alert
        function deleteRecord(id) {
            confirmDelete(id);
        }
        $('.deleteRecord').on('click', function() {
            var id = $(this).attr('rel');
            confirmDelete(id);
        });

        $('#test-datatable').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ url('backend/orders/data')}}",
            columns: [
                {"data":'id'},
                {"data":'order_date'},
                {"data":'order_time'},
                {"data":'name_surename'},
                {"data":'tel'},
                {"data":'address'},
                {"data": 'districts'},
                {"data":'zipcode'},
                {"data": 'product'},
                {"data":'payment'},
                {"data": 'total_price', render: $.fn.dataTable.render.number( ',', '.', 0 ) },
                {"data": 'purchase_channel'},
                {"data": 'profile'},
                {"data": null ,orderable: false, searchable: false}
            ],
            order: [[ 0, "desc" ]],
            columnDefs: [
                    // { "orderable": false, "targets": 13},
                    {className: "text-center", "targets": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 13]},
                    {className: "text-right", "targets": [10]},
                    {
                        "render": function (data, type, row, meta) {
                            dataReturn = '';
                            dataReturn += '<ul class="icons-list">';
                            dataReturn += '	<li class="dropdown">';
                            dataReturn += '		<a href="#" class="dropdown-toggle" data-toggle="dropdown">';
                            dataReturn += '			<i class="icon-menu9"></i>';
                            dataReturn += '		</a>';

                            dataReturn += '		<ul class="dropdown-menu dropdown-menu-right">';
                            dataReturn += '			<li><a href="{{ url('backend/orders')}}/' + data.id + '/edit"><i class="icon-pencil7"></i>แก้ไข</a></li>';
                            dataReturn += '			<li><a href="{{ url('backend/claim-orders')}}/' + data.id + '"><i class="icon-forward"></i>เคลม</a></li>';
                            dataReturn += '			<li><a href="javascript:" onclick="deleteRecord('+ data.id +')"><i class="icon-bin"></i>ลบ</a></li>';

                            dataReturn += '		</ul>';
                            dataReturn += '	</li>';
                            dataReturn += '</ul>';

                            return dataReturn;
                        },
                        "targets": 13
                    },
                ],
            });
        });

    function deleteRecord(id) {
            confirmDelete(id);
    }

    function confirmDelete(id) {
            swal({
                title: "Confirm Delete",
                text: "Are you sure you want to delete this Order?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }, function (isConfirm) {
                if (!isConfirm) return;
                $.ajax({
                    url : "{{ url('backend/orders')}}" + '/' + id,
                    type : "POST",
                    data : {'_method' : 'DELETE'},
                    success: function () {
                        swal({title: "Success", text: "Order has been deleted! \nClick 'Ok' to refresh page.", type: "success"},
                            function(){
                                location.reload();
                            }
                        );
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        swal("Error!", "Please try again", "error");
                    }
                });
            });
    }
</script>

@endsection
