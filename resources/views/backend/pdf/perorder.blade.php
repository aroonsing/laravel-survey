<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>พิมพ์ใบสั่งซื้อราย Order</title>
<style>
    @font-face {
        font-family: 'THSarabunNew';
        font-style: normal;
        font-weight: normal;
        src: url("{{ asset('fonts/THSarabunNew.ttf') }}") format('truetype');
    }

    @font-face {
        font-family: 'THSarabunNew';
        font-style: normal;
        font-weight: bold;
        src: url("{{ asset('fonts/THSarabunNew Bold.ttf') }}") format('truetype');
    }

    @font-face {
        font-family: 'THSarabunNew';
        font-style: italic;
        font-weight: normal;
        src: url("{{ asset('fonts/THSarabunNew Italic.ttf') }}") format('truetype');
    }

    @font-face {
        font-family: 'THSarabunNew';
        font-style: italic;
        font-weight: bold;
        src: url("{{ asset('fonts/THSarabunNew BoldItalic.ttf') }}") format('truetype');
    }

    body {
        font-family: "THSarabunNew";
        font-size: 20px;
        font-weight: bold;
    }

    table {
        line-height: 15px;
    }

    .table-print-box {
        line-height: 15px;
        height: 96%;
        max-height: 96%;
        margin: auto;
        /* border: 1px solid #000000; */
        width: 100%;
        text-align: center;
    }

    .table-print-head {
        line-height: 15px;
        margin: auto;
        /* border: 1px solid #000000; */
        width: 100%;
        text-align: center;
    }

    .table-print-panel {
        margin: auto;
        /* border: 1px solid #000000; */
        width: 80%;
        text-align: center;
    }

    .table-print-panel td {
        padding-left: 5px;
        padding-right: 5px;
        /* border: 1px solid #000000; */
    }


    .text-left {
        text-align: left;
    }

    .text-right {
        text-align: right;
    }

    .text-center {
        text-align: center;
    }

    .page-break {
        page-break-after: always;
    }

    img {
        width: 150px;
        height: auto;
    }
</style>


<body>
    @foreach ($orders as $key => $order)
    <br>
    <br>
    <br>
    <br>
    <table class="table-print-head">
        <tr>
            <td class="text-left">
                ผู้ส่ง<br>
                {{-- {{ Auth::user()->name }}<br> --}}
                {{-- โทร. 0651945222 (คุณฝ้าย) 0967222210 (คุณเรียว)<br>
                29/2 หมู่ 2 ตำบลศาลาขาว อำเภอเมือง จังหวัด สุพรรบุรี 72210 --}}
                {{ $order->sender_name }}<br>
                {!! nl2br($order->sender_description) !!}
            </td>
            <td class="text-alin">
                <img src="{{ asset('/images/payment/'.$order->payment.'.jpg')}}" width="500" height="375">
            </td>
        </tr>
    </table>
    <br>
    <br>
    <br>
    <br>
    <br>
    <table class="table-print-panel">
        <tr>
            <td colspan="2">ผู้รับ</td>
        </tr>
        <tr>
            <td class="text-right">ชื่อ-นามสกุล</td>
            <td>{{$order->name_surename}}</td>
        </tr>
        <tr>
            <td class="text-right">เบอร์โทรศัพท์</td>
            <td>{{$order->tel}}</td>
        </tr>
        <tr>
            <td class="text-right">ที่อยู่</td>
            <td>
                {{$order->address}}<br>
                {{$order->districts_name}} {{$order->amphures_name}} {{$order->provinces_name}}
                {{$order->zip_code}}
            </td>
        </tr>
        <tr>
            <td class="text-right">รายการสินค้า</td>
            <td>{{$order->product_name}} {{$order->color}}
                @if ($order->payment == 'COD')
                {{$order->payment}} {{$order->total_price}} บาท
                @endif
            </td>
        </tr>
    </table>
    @endforeach
</body>

</html>
