@extends('layouts.limitless.index')

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        <h5 class="panel-title">Role Management<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
        <div class="heading-elements">
            {{-- @can('role-create') --}}
            <a class="btn btn-success heading-btn" href="{{ route('roles.create') }}"> Create New Role</a>
            {{-- @endcan --}}
        </div>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-bordered table-xxs">
                    <thead>
                        <tr class='bg-grey-600'>
                            <th width="50px">#</th>
                            <th>Name</th>
                            <th width="280px">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($roles as $key => $role)
                        <tr>
                            <td>{{ ++$i }}</td>
                            <td>{{ $role->name }}</td>
                            <td>
                                <a href="{{ route('roles.show',$role->id) }}" class="label bg-primary"><i class="icon-file-text position-left"></i>Show</a>
                                {{-- @can('role-edit') --}}
                                <a href="{{ route('roles.edit',$role->id) }}" class="label bg-green"><i class="icon-pencil7 position-left"></i>Edit</a>
                                {{-- @endcan                       --}}
                                {{-- @can('role-delete') --}}
                                    {!! Form::open(['method' => 'DELETE','route' => ['roles.destroy', $role->id],'style'=>'display:inline']) !!}
                                        {!! Form::submit('Delete', ['class' => 'label bg-danger','style'=>'display:inline']) !!}
                                    {!! Form::close() !!}
                                {{-- @endcan --}}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
            </table>
        </div>
    </div>
    <div class="panel-footer"><a class="heading-elements-toggle"><i class="icon-more"></i></a>
            <div class="heading-elements">
                <span class="heading-text text-semibold">Pagination:</span>
                {!! $roles->links('layouts.limitless.partials.pagination.default') !!} 
            </div>
    </div>
</div>

@if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
@endif

@endsection