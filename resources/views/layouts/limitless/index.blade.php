<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1"> -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>
        @if(View::hasSection('title'))
        @yield('title')
        @else
        {{ config('app.name', 'Testttt') }}
        @endif
    </title>
    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
        type="text/css">
    <link href="{{ asset('assets_backend/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets_backend/css/bootstrap.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets_backend/css/core.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets_backend/css/components.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets_backend/css/colors.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets_backend/css/style.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://earthchie.github.io/jquery.Thailand.js/jquery.Thailand.js/dist/jquery.Thailand.min.css">
    <!-- /global stylesheets -->
    @stack('styles')

    <!-- Core JS files -->
    <script type="text/javascript" src="{{ asset('assets_backend/js/plugins/loaders/pace.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets_backend/js/core/libraries/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets_backend/js/core/libraries/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets_backend/js/plugins/loaders/blockui.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets_backend/js/plugins/extensions/cookie.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets_backend/js/plugins/notifications/pnotify.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets_backend/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets_backend/js/plugins/tables/datatables/extensions/buttons.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets_backend/js/plugins/tables/datatables/extensions/jszip/jszip.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets_backend/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets_backend/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js')}}"></script>

    <script type="text/javascript" src="{{ asset('assets_backend/js/plugins/forms/selects/select2.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets_backend/js/plugins/notifications/sweet_alert.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets_backend/js/plugins/forms/editable/editable.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets_backend/js/core/app.js')}}"></script>

    <script type="text/javascript" src="https://earthchie.github.io/jquery.Thailand.js/jquery.Thailand.js/dependencies/JQL.min.js"></script>
    <script type="text/javascript" src="https://earthchie.github.io/jquery.Thailand.js/jquery.Thailand.js/dependencies/typeahead.bundle.js"></script>
    <script type="text/javascript" src="https://earthchie.github.io/jquery.Thailand.js/jquery.Thailand.js/dist/jquery.Thailand.min.js"></script>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.7.3/handlebars.min.js"></script>

    <!-- /core JS files -->

    <!-- Theme JS files -->
    @yield('extra-scripts')
    <!-- /theme JS files -->

</head>

<body class="navbar-top">
    <!-- Main navbar -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="navbar-header">
            <a class="navbar-brand" href="/"><img src="{{ asset('assets_backend/images/logo_light.png') }}" alt=""></a>

            <ul class="nav navbar-nav visible-xs-block">
                <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
                <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
            </ul>
        </div>

        <div class="navbar-collapse collapse" id="navbar-mobile">
            <ul class="nav navbar-nav">
                <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                {{-- <li class="dropdown language-switch">
                    <a class="dropdown-toggle" data-toggle="dropdown">
                        @php $flage = trans('locale.lang'); @endphp

                        <img src="{{ asset('assets_backend/images/flags/'.$flage.'.png')}}" class="position-left"
                            alt="">
                        {{__('locale.language')}}
                        <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu">
                        <li><a href="{{ URL::to('locale/th') }}" class="thai"><img
                                    src="{{ asset('assets_backend/images/flags/th.png') }}" alt=""> ภาษาไทย</a></li>
                        <li><a href="{{ URL::to('locale/en') }}" class="english"><img
                                    src="{{ asset('assets_backend/images/flags/gb.png') }}" alt=""> English</a></li>

                    </ul>
                </li> --}}
                <li class="dropdown dropdown-user">
                    <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                        <img src="{{ asset('assets_backend/images/placeholder.jpg') }}" alt="">
                        <span>{{ Auth::user()->name }}</span>
                        <i class="caret"></i>
                    </a>

                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="{{ url('/auth/settings') }}"><i class="icon-user-plus"></i>Change Password</a>
                        </li>
                        {{-- <li><a href="#"><i class="icon-coins"></i> My balance</a></li>
						<li><a href="#"><span class="badge bg-teal-400 pull-right">58</span> <i class="icon-comment-discussion"></i> Messages</a></li> --}}
                        <li class="divider"></li>
                        {{-- <li><a href="#"><i class="icon-cog5"></i> Account settings</a></li> --}}
                        <li><a href="{{ url('/signout') }}" onclick="event.preventDefault();
							document.getElementById('logout-form').submit();"><i class="icon-switch2"></i> Logout</a></li>
                        <form id="logout-form" action="{{ url('/signout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <!-- /main navbar -->


    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main sidebar -->
            @include('layouts.limitless.partials.navmenu')
            <!-- /main sidebar -->


            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Page header -->
                {{-- <div class="page-header page-header-default">
                    <div class="breadcrumb-line">
                        @role('admin')
                        {!! Menu::get('adminNavBar')->crumbMenu()->asUl(['class' => 'breadcrumb']) !!}
                        @endrole
                    </div>
                </div> --}}
                <!-- /page header -->


                <!-- Content area -->

                <div class="content">
                    <!-- Colored table options -->
                    @yield('content')
                    <!-- /colored table options -->
                </div>
                <!-- /content area -->

            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->

    </div>
    <!-- /page container -->

    <!--[if lt IE 11]>
	<p class="browserupgrade">
		โปรมของคุณไม่รองรับกรุณาอัพเกรดใหม่
	</p>
	<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please
		<a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.
	</p>
	<style>
		.browserupgrade {
			font-size: 2em;
			text-align: center;
			color: red;
		}

		.wrapper {
			display: none;
		}
	</style>
	<![endif]-->
</body>

</html>
