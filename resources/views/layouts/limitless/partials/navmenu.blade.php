<div class="sidebar sidebar-main">
    <div class="sidebar-content">
        <!-- Main navigation -->
        <div class="sidebar-category sidebar-category-visible">
            <div class="category-content no-padding">

                @role('admin')
                {!! $adminNavBar->asUl( ['class' => 'navigation navigation-main navigation-accordion'] ) !!}
                @endrole
            </div>
        </div>
        <!-- /main navigation -->
    </div>
</div>
