<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Traits\HasRoles;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Role::create(['name' => 'admin']);
        Role::create(['name' => 'partner']);

        $useradmin = App\User::create([
            'name' => 'Admin',
            'username' => 'admin',
            'password' => bcrypt('aa123456'),
        ]);

        $userpartner = App\User::create([
            'name' => 'Partner1',
            'username' => 'partner1',
            'password' => bcrypt('aa123456'),
        ]);

        $useradmin->assignRole([1]);
        $userpartner->assignRole([2]);
    }
}
