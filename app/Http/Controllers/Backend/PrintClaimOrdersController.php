<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use App\ClaimOrder;
use PDF;
use DB;
use Datatables;

class PrintClaimOrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.print.claim_orders');
    }

    public function indexData()
    {

        $data = ClaimOrder::select(
            'claim_orders.id',
            DB::raw("DATE_FORMAT(claim_orders.updated_at, '%d/%m/%Y') AS order_date"),
            DB::raw("DATE_FORMAT(claim_orders.updated_at, '%H:%i:%s') AS order_time"),
            'claim_orders.claim_name',
            'claim_orders.description',
            'claim_orders.name_surename',
            'claim_orders.tel',
            'claim_orders.address',
            DB::raw("CONCAT(claim_orders.districts,' ', claim_orders.amphures,' ', claim_orders.provinces) AS districts"),
            'claim_orders.zipcode',
            'claim_orders.total_price',
            'claim_orders.status_print'
        );

        return Datatables::of($data)
            ->filterColumn('order_date', function ($query, $keyword) {
                $query->whereRaw("DATE_FORMAT(claim_orders.updated_at, '%d/%m/%Y') like ?", ["%{$keyword}%"]);
            })
            ->filterColumn('order_time', function ($query, $keyword) {
                $query->whereRaw("DATE_FORMAT(claim_orders.updated_at, '%H:%i:%s') like ?", ["%{$keyword}%"]);
            })
            ->filterColumn('districts', function ($query, $keyword) {
                $query->whereRaw("CONCAT(claim_orders.districts,' ', claim_orders.amphures,' ', claim_orders.provinces) like ?", ["%{$keyword}%"]);
            })
            ->make();
    }

    public function perDay(Request $request)
    {
        // DB::connection()->enableQueryLog();
        $data = ClaimOrder::groupBy(DB::raw("CAST(claim_orders.updated_at AS DATE)"))
            ->select(
                DB::raw('count(claim_orders.id) AS order_count'),
                DB::raw("DATE_FORMAT(claim_orders.updated_at, '%Y-%m-%d') AS order_date_db"),
                DB::raw("DATE_FORMAT(claim_orders.updated_at, '%d/%m/%Y') AS order_date")
            )->orderBy('claim_orders.updated_at', 'DESC')->paginate(50);

        // $queries = DB::getQueryLog();
        // dd($queries);

        return view('backend.printclaimorders.perday', compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 50);
    }

    function pdfPerOrder($id)
    {
        $orders = ClaimOrder::join('orders', 'orders.id', '=', 'claim_orders.order_id')
            ->join('products', 'products.id', '=', 'orders.product_id')
            ->join('products_attributes', 'products_attributes.id', '=', 'orders.products_attributes_id')
            ->join('senders', 'senders.id', '=', 'orders.sender_id')
            ->where('claim_orders.id', '=', $id)
            ->select(
                'orders.id',
                'senders.sender_name',
                'senders.description as sender_description',
                'claim_orders.claim_name',
                'claim_orders.name_surename',
                'claim_orders.tel',
                'claim_orders.address',
                'claim_orders.provinces AS provinces_name',
                'claim_orders.amphures AS amphures_name',
                'claim_orders.districts AS districts_name',
                'claim_orders.zipcode AS zip_code',
                'products.product_name',
                'products_attributes.color'
            )->orderBy('claim_orders.updated_at', 'DESC')->get();

        ClaimOrder::where('claim_orders.id', '=', $id)
            ->update([
                'status_print' => 1
            ]);
        // dd($orders);

        $pdf = PDF::loadView('backend.pdf.claimperorder', ['orders' => $orders])->setPaper('a4');
        return $pdf->stream();
    }

    function pdfselectedOrder(Request $request)
    {

        $data = $request->all();
        // DB::connection()->enableQueryLog();
        $orders =  ClaimOrder::join('orders', 'orders.id', '=', 'claim_orders.order_id')
            ->join('products', 'products.id', '=', 'orders.product_id')
            ->join('products_attributes', 'products_attributes.id', '=', 'orders.products_attributes_id')
            ->join('senders', 'senders.id', '=', 'orders.sender_id')
            ->whereIn('claim_orders.id', $data['checkitem'])
            ->select(
                'orders.id',
                'senders.sender_name',
                'senders.description as sender_description',
                'claim_orders.claim_name',
                'claim_orders.name_surename',
                'claim_orders.tel',
                'claim_orders.address',
                'claim_orders.provinces AS provinces_name',
                'claim_orders.amphures AS amphures_name',
                'claim_orders.districts AS districts_name',
                'claim_orders.zipcode AS zip_code',
                'products.product_name',
                'products_attributes.color'
            )->orderBy('claim_orders.updated_at', 'DESC')->get();

        ClaimOrder::whereIn('claim_orders.id', $data['checkitem'])
            ->update([
                'status_print' => 1
            ]);
        // $queries = DB::getQueryLog();
        // dd($queries);

        $pdf = PDF::loadView('backend.pdf.claimperday', ['orders' => $orders])->setPaper('a4');
        return $pdf->stream();
    }
}
