<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\ClaimOrder;
use App\Order;
use DB;
use Datatables;

class ClaimOrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        return view('backend.claimorders.create', compact('id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $this->validate($request, [
            "claim_name" => "required",
            "name_surename" => "required",
            "tel" => "required|numeric|digits:10",
            "address" => "required",
            "provinces" => "required",
            "amphures" => "required",
            "districts" => "required",
            "zipcode" => "required",
            "total_price" => "required|regex:/^\d+(\.\d{1,2})?$/",
        ]);

        $data = $request->all();

        $order = new ClaimOrder();
        $order->order_id =          $data['order_id'];
        $order->claim_name =        $data['claim_name'];
        $order->description =       $data['description'];
        $order->name_surename =       $data['name_surename'];
        $order->tel =               $data['tel'];
        $order->address =           $data['address'];
        $order->provinces =         $data['provinces'];
        $order->amphures =          $data['amphures'];
        $order->districts =         $data['districts'];
        $order->zipcode =           $data['zipcode'];
        $order->total_price =       $data['total_price'];
        $order->created_user_id =   Auth::user()->id;

        $order->save();

        $order_id = $data['order_id'];

        return redirect()->route('claim-orders.show', [$order_id])
            ->with('success', 'Claim has been added Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order_id = $id;
        $data = Order::join('products', 'products.id', '=', 'orders.product_id')
            ->join('products_attributes', 'products_attributes.id', '=', 'orders.products_attributes_id')
            ->select(
                'orders.id',
                DB::raw("DATE_FORMAT(orders.updated_at, '%d/%m/%Y') AS order_date"),
                DB::raw("DATE_FORMAT(orders.updated_at, '%H:%i:%s') AS order_time"),
                'orders.name_surename',
                'orders.tel',
                'orders.address',
                DB::raw("CONCAT(districts,' ', amphures,' ', provinces) AS districts"),
                'zipcode AS zip_code',
                DB::raw("CONCAT(products.product_name,' ', products_attributes.color) AS product"),
                'orders.payment',
                'orders.total_price',
                'orders.purchase_channel',
                'orders.profile',
                'orders.created_at',
                'orders.updated_at',
                'orders.deleted_at'
            )->orderBy('orders.id', 'DESC');
        $data = $data->where('orders.id', '=', $order_id)->first();
        return view('backend.claimorders.index', compact('order_id', 'data'));
    }

    public function showData($id)
    {

        $data = ClaimOrder::where('claim_orders.order_id', '=', $id)
            ->select(
                'claim_orders.id',
                DB::raw("DATE_FORMAT(claim_orders.updated_at, '%d/%m/%Y') AS order_date"),
                DB::raw("DATE_FORMAT(claim_orders.updated_at, '%H:%i:%s') AS order_time"),
                'claim_orders.claim_name',
                'claim_orders.description',
                'claim_orders.name_surename',
                'claim_orders.tel',
                'claim_orders.address',
                DB::raw("CONCAT(claim_orders.districts,' ', claim_orders.amphures,' ', claim_orders.provinces) AS districts"),
                'claim_orders.zipcode AS zip_code',
                'claim_orders.total_price'
            )
            ->orderBy('claim_orders.id', 'DESC');

        $data = $data->get();

        return Datatables::of($data)->make();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $claim = ClaimOrder::find($id);
        // dd($claim);
        return view('backend.claimorders.edit', compact('claim'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();

        $this->validate($request, [
            "claim_name" => "required",
            "name_surename" => "required",
            "tel" => "required|numeric|digits:10",
            "address" => "required",
            "provinces" => "required",
            "amphures" => "required",
            "districts" => "required",
            "zipcode" => "required",
            "total_price" => "required|regex:/^\d+(\.\d{1,2})?$/",
        ]);

        ClaimOrder::where(['id' => $id])
            ->update([
                'claim_name'    =>  $data['claim_name'],
                'description'   =>  $data['description'],
                'name_surename' =>  $data['name_surename'],
                'tel'           =>  $data['tel'],
                'address'       =>  $data['address'],
                'provinces'     =>  $data['provinces'],
                'amphures'      =>  $data['amphures'],
                'districts'     =>  $data['districts'],
                'zipcode'       =>  $data['zipcode'],
                'total_price'   =>  $data['total_price']
            ]);

        $order_id = $data['order_id'];

        return redirect()->route('claim-orders.show', [$order_id])
            ->with('success', 'Claim Updated Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
