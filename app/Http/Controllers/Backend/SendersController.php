<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Sender;
use Datatables;

class SendersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.senders.index');
    }

    public function indexData()
    {
        $user = Auth::user()->getRoleNames();

        $data = Sender::where('created_user_id', '=', Auth::user()->id)->get();

        return Datatables::of($data)->make();

        // return view('backend.orders.index_datatables', compact('data'))
        //     ->with('i', ($request->input('page', 1) - 1) * 50);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.senders.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'sender_name' => 'required',
            'description' => 'required'
        ]);

        $data = $request->all();
        $sender = new Sender;
        $sender->sender_name        = $data['sender_name'];
        $sender->created_user_id     = Auth::user()->id;
        if (!empty($data['description'])) {
            $sender->description = $data['description'];
        } else {
            $sender->description  = '';
        }
        $sender->save();

        return redirect()->route('senders.index')
            ->with('success', 'Sender has been added Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sender = Sender::find($id);
        return view('backend.senders.edit', compact('sender'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'sender_name' => 'required',
            'description' => 'required'
        ]);

        $data = $request->all();

        Sender::where(['id' => $id])
            ->update([
                'sender_name'   =>    $data['sender_name'],
                'description'   =>    $data['description']

            ]);

        return redirect()->route('senders.index')
            ->with('success', 'Sender Updated Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Sender::find($id)->delete();
    }
}
