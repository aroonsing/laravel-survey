<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\WorkingDay;
use Datatables;
use Illuminate\Support\Facades\Redirect;

class WorkingDaysController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.working_days.index');
    }

    public function indexData()
    {

        $data = WorkingDay::get();
        return Datatables::of($data)->make();

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = WorkingDay::where('status_closed','=','0')->first();
        if($data != null){
            return Redirect::back()->withErrors('กรุณาปิดวันทำการก่อนหน้าให้เรียบร้อย');
        }
        return view('backend.working_days.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'date_working' => 'required',
        ]);

        $data = $request->all();

        $WorkingDay = new WorkingDay;
        $WorkingDay->date_working = $data['date_working'];
        $WorkingDay->status_closed = 0;
        $WorkingDay->created_user_id = Auth::user()->id;

        $WorkingDay->save();

        return redirect()->route('working-days.index')
            ->with('success', 'เพิ่มวันทำการเรียบร้อย!');



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        WorkingDay::where(['id' => $id])
            ->update([
                'status_closed'   =>    1
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
