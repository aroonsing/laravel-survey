<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\ClaimOrder;
use DB;

class ReportClaimOrdersController extends Controller
{
    function index(Request $request)
    {
        return view('backend.reportclaimorders.index');
    }

    function indexData(Request $request)
    {

        DB::connection()->enableQueryLog();

        $user = Auth::user()->getRoleNames();

        $data = ClaimOrder::select(
            'claim_orders.id',
            DB::raw("DATE_FORMAT(claim_orders.updated_at, '%d/%m/%Y') AS order_date"),
            DB::raw("DATE_FORMAT(claim_orders.updated_at, '%H:%i:%s') AS order_time"),
            'claim_orders.claim_name',
            'claim_orders.description',
            'claim_orders.name_surename',
            'claim_orders.tel',
            'claim_orders.address',
            DB::raw("CONCAT(claim_orders.districts,' ', claim_orders.amphures,' ', claim_orders.provinces) AS districts"),
            'claim_orders.zipcode AS zip_code',
            'claim_orders.total_price'
        )
            ->orderBy('claim_orders.id', 'DESC');

        if ($user[0] != 'admin') {
            $data = $data->where('created_user_id', '=', Auth::user()->id);
        }

        if (!empty($request->from_date)) {
            $data = $data->whereBetween(DB::raw('DATE(orders.created_at)'), array($request->from_date, $request->to_date));
        }

        $data = $data->get();

        return datatables()->of($data)->make(true);
    }
}
