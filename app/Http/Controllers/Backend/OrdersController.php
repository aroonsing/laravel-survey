<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\OrdersPostRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Product;
use App\ProductsAttribute;
use App\Order;
use App\Sender;
use App\Stock;
use App\WorkingDay;
use DB;
use Datatables;
use Illuminate\Support\Facades\Redirect;


// use OrdersPostRequest;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('backend.orders.index');
    }

    public function indexData()
    {
        // dd($request);
        $user = Auth::user()->getRoleNames();

        $data = Order::join('products', 'products.id', '=', 'orders.product_id')
            ->join('products_attributes', 'products_attributes.id', '=', 'orders.products_attributes_id')
            ->select([
                'orders.id',
                DB::raw("DATE_FORMAT(orders.updated_at, '%d/%m/%Y') AS order_date"),
                DB::raw("DATE_FORMAT(orders.updated_at, '%H:%i:%s') AS order_time"),
                'orders.name_surename',
                'orders.tel',
                'orders.address',
                DB::raw("CONCAT(districts,' ', amphures,' ', provinces) AS districts"),
                'zipcode',
                DB::raw("CONCAT(products.product_name,' ', products_attributes.color) AS product"),
                'orders.payment',
                'orders.total_price',
                'orders.purchase_channel',
                'orders.profile'
            ])/*->orderBy('orders.id', 'DESC')*/;

        if ($user[0] != 'admin') {
            $data = $data->where('created_user_id', '=', Auth::user()->id);
        }

        return Datatables::of($data)
            ->filterColumn('order_date', function ($query, $keyword) {
                $query->whereRaw("DATE_FORMAT(orders.updated_at, '%d/%m/%Y') like ?", ["%{$keyword}%"]);
            })
            ->filterColumn('order_time', function ($query, $keyword) {
                $query->whereRaw("DATE_FORMAT(orders.updated_at, '%H:%i:%s') like ?", ["%{$keyword}%"]);
            })
            ->filterColumn('product', function ($query, $keyword) {
                $query->whereRaw("CONCAT(products.product_name,' ', products_attributes.color) like ?", ["%{$keyword}%"]);
            })
            ->filterColumn('districts', function ($query, $keyword) {
                $query->whereRaw("CONCAT(districts,' ', amphures,' ', provinces) like ?", ["%{$keyword}%"]);
            })
            ->make();

        // return view('backend.orders.index_datatables', compact('data'))
        //     ->with('i', ($request->input('page', 1) - 1) * 50);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = Product::get();
        $workingDay = WorkingDay::where('status_closed', '=', 0)->first();
        $senders = Sender::get()->where('created_user_id', '=', Auth::user()->id);
        return view('backend.orders.create', compact('products', 'senders', 'workingDay'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OrdersPostRequest $request)
    {

        $data = $request->all();

        $stock = Stock::orderBy('stocks.id', 'ASC')
            ->where([
                ['product_id', '=', $data['product_id']],
                ['products_attributes_id', '=', $data['products_attributes_id']],
                ['stock_balance', '>', 0]
            ])->first();

        $workingDay = WorkingDay::where('status_closed', '=', 0)->first();

        if ($workingDay == null) {
            return Redirect::back()->withErrors('กรุณาเปิดวันทำการ!')->withInput($request->all());
        }



        if ($stock == null) {
            return Redirect::back()->withErrors('ไม่มีสินค้าใน Stock!')->withInput($request->all());
        }
        // dd($stock == null);

        $order = new Order;
        $order->working_days_id =           $workingDay->id;
        $order->sender_id =                 $data['sender_id'];
        $order->name_surename =             $data['name_surename'];
        $order->tel =                       $data['tel'];
        $order->address =                   $data['address'];
        $order->provinces =                 $data['provinces'];
        $order->amphures =                  $data['amphures'];
        $order->districts =                 $data['districts'];
        $order->zipcode =                   $data['zipcode'];
        $order->product_id =                $data['product_id'];
        $order->products_attributes_id =    $data['products_attributes_id'];
        $order->payment =                   $data['payment'];
        $order->total_price =               $data['total_price'];
        $order->purchase_channel =          $data['purchase_channel'];
        $order->profile =                   $data['profile'];
        $order->stock_id =                  $stock->id;
        $order->price_cost =                $stock->price_cost;
        $order->created_user_id =           Auth::user()->id;
        $order->status_print =              0;


        $order->save();

        ProductsAttribute::where(['id' => $data['products_attributes_id']])
            ->update([
                'stock' => DB::raw('stock-1')
            ]);

        Stock::where([
            ['id', '=', $stock->id]
        ])->update([
            'stock_balance' => DB::raw('stock_balance-1')
        ]);


        return redirect()->route('orders.index')
            ->with('success', 'Order has been added Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order = Order::find($id);
        $products = Product::get();
        $senders = Sender::get()->where('created_user_id', '=', Auth::user()->id);
        return view('backend.orders.edit', compact('order', 'products', 'senders'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(OrdersPostRequest $request, $id)
    {
        $data = $request->all();

        $stock = Stock::orderBy('stocks.id', 'ASC')
            ->where([
                ['product_id', '=', $data['product_id']],
                ['products_attributes_id', '=', $data['products_attributes_id']],
                ['stock_balance', '>', 0]
            ])->first();


        if ($stock == null) {
            return Redirect::back()->withErrors('ไม่มีสินค้าใน Stock!')->withInput($request->all());
        }

        $beforeOrder = Order::find($id);



        Order::where(['id' => $id])
            ->update([
                'sender_id'                 =>    $data['sender_id'],
                'name_surename'             =>    $data['name_surename'],
                'tel'                       =>    $data['tel'],
                'address'                   =>    $data['address'],
                'provinces'                 =>    $data['provinces'],
                'amphures'                  =>    $data['amphures'],
                'districts'                 =>    $data['districts'],
                'zipcode'                   =>    $data['zipcode'],
                'product_id'                =>    $data['product_id'],
                'products_attributes_id'    =>    $data['products_attributes_id'],
                'payment'                   =>    $data['payment'],
                'total_price'               =>    $data['total_price'],
                'purchase_channel'          =>    $data['purchase_channel'],
                'stock_id'                  =>    $stock->id,
                'profile'                   =>    $data['profile']
            ]);

        if (
            $beforeOrder->product_id != $data['product_id'] ||
            $beforeOrder->products_attributes_id != $data['products_attributes_id']
        ) {

            ProductsAttribute::where(['id' => $data['products_attributes_id']])
                ->update([
                    'stock' => DB::raw('stock-1')
                ]);

            ProductsAttribute::where(['id' => $beforeOrder->products_attributes_id])
                ->update([
                    'stock' => DB::raw('stock+1')
                ]);

            Stock::where([
                ['id', '=', $stock->id]
            ])->update([
                'stock_balance' => DB::raw('stock_balance-1')
            ]);

            Stock::where([
                ['id', '=', $beforeOrder->stock_id]
            ])->update([
                'stock_balance' => DB::raw('stock_balance+1')
            ]);
        }



        return redirect()->route('orders.index')
            ->with('success', 'Order Updated Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $beforeOrder = Order::find($id);

        Order::find($id)->delete();


        ProductsAttribute::where(['id' => $beforeOrder->products_attributes_id])
            ->update([
                'stock' => DB::raw('stock+1')
            ]);

        Stock::where([
            ['id', '=', $beforeOrder->stock_id]
        ])->update([
            'stock_balance' => DB::raw('stock_balance+1')
        ]);
    }
}
