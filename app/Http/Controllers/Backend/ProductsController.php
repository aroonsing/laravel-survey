<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Image;
use App\Product;
use App\ProductsAttribute;
use Datatables;


class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('backend.products.index');
    }

    public function indexData()
    {
        $data = Product::select(
                "id",
                "product_name",
                "product_code",
                "image"
            );

        return Datatables::of($data)
        ->addColumn('details_url', function($data) {
            return url('backend/products/details-data/' . $data->id);
        })
        ->make();

    }

    public function indexDataDetail($id)
    {
        $products_attributes = ProductsAttribute::where('product_id', $id);
        return Datatables::of($products_attributes)->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.products.create'/*, compact('roles')*/);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'product_name' => 'required',
            'product_code' => 'required'
        ]);
        $data = $request->all();

        $product = new Product;
        $product->product_name = $data['product_name'];
        $product->product_code = $data['product_code'];
        if (!empty($data['description'])) {
            $product->description = $data['description'];
        } else {
            $product->description  = '';
        }

        //Upload image
        if ($request->hasFile('image')) {
            $image_tmp = $request->file('image');
            if ($image_tmp->isValid()) {
                $extention = $image_tmp->getClientOriginalExtension();
                $filename = rand(111, 99999) . '.' . $extention;
                $large_image_path = 'images/backend/products/large/' . $filename;
                $medium_image_path = 'images/backend/products/medium/' . $filename;
                $small_image_path = 'images/backend/products/small/' . $filename;

                Image::make($image_tmp)->save($large_image_path);
                Image::make($image_tmp)->resize(600, 600)->save($medium_image_path);
                Image::make($image_tmp)->resize(300, 300)->save($small_image_path);

                $product->image = $filename;
            }
        }

        $product->save();

        return redirect()->route('products.index')
            ->with('success', 'Product has been added Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);

        return view('backend.products.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'product_name' => 'required',
            'product_code' => 'required'
        ]);

        $data = $request->all();

        if (empty($data['description'])) {
            $data['description']  = '';
        }

        //Upload image
        if ($request->hasFile('image')) {
            $image_tmp = $request->file('image');
            if ($image_tmp->isValid()) {
                $extention = $image_tmp->getClientOriginalExtension();
                $filename = rand(111, 99999) . '.' . $extention;
                $large_image_path = 'images/backend/products/large/' . $filename;
                $medium_image_path = 'images/backend/products/medium/' . $filename;
                $small_image_path = 'images/backend/products/small/' . $filename;

                Image::make($image_tmp)->save($large_image_path);
                Image::make($image_tmp)->resize(600, 600)->save($medium_image_path);
                Image::make($image_tmp)->resize(300, 300)->save($small_image_path);
            }
        } else {
            $filename = $data['current_image'];
        }

        Product::where(['id' => $id])
            ->update([
                'product_name' => $data['product_name'],
                'product_code' => $data['product_code'],
                'description' => $data['description'],
                'image' => $filename
            ]);

        return redirect()->route('products.index')
            ->with('success', 'Product Updated Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::find($id)->delete();
        return redirect()->route('products.index')
            ->with('success', 'Product has been deleted Successfully!');
    }


    public function deleteImage($id = null)
    {
        Product::where(['id' => $id])->update(['image' => '']);
        return redirect()->back()->with('success', 'Product Image has been deleted Successfully!');
    }

    public function addAttributes(Request $request, $id = null)
    {
        $productDetails = Product::with('attributes')->where(['id' => $id])->first();

        if ($request->isMethod('post')) {
            $data = $request->all();
            foreach ($data['sku'] as $key => $val) {

                if (!empty($val)) {
                    $attribute = new ProductsAttribute;
                    $attribute->product_id = $id;
                    $attribute->sku = $val;
                    $attribute->color = $data['color'][$key];
                    $attribute->stock = 0;
                    $attribute->save();
                }
            }

            return redirect('backend/products/add-attributes/' . $id)->with('flash_message_success', 'Product Attributes has been added Successfully!');
        }

        return view('backend.products.add_attributes')->with(compact('productDetails'));
    }

    public function deleteAttribute($id = null)
    {
        ProductsAttribute::where(['id' => $id])->delete();
        return redirect()->back()->with('flash_message_success', 'Attribute has been deleted Successfully!');
    }
}
