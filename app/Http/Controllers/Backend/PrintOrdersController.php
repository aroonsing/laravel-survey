<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use App\Order;
use PDF;
use DB;
use Datatables;

class PrintOrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.print.orders');
    }

    public function indexData(Request $request)
    {

        $data = Order::join('products', 'products.id', '=', 'orders.product_id')
            ->join('products_attributes', 'products_attributes.id', '=', 'orders.products_attributes_id')
            ->join('working_days', 'working_days.id', '=', 'orders.working_days_id')
            ->select(
                'orders.id',
                DB::raw("DATE_FORMAT(working_days.date_working, '%d/%m/%Y') AS working_date"),
                DB::raw("DATE_FORMAT(orders.created_at, '%d/%m/%Y') AS order_date"),
                DB::raw("DATE_FORMAT(orders.created_at, '%H:%i:%s') AS order_time"),
                'orders.name_surename',
                'orders.tel',
                'orders.address',
                DB::raw("CONCAT(districts,' ', amphures,' ', provinces) AS districts"),
                'zipcode',
                DB::raw("CONCAT(products.product_name,' ', products_attributes.color) AS product"),
                'orders.payment',
                'orders.total_price',
                'orders.purchase_channel',
                'orders.profile',
                'orders.status_print'
            )/*->orderBy('orders.updated_at', 'DESC')->paginate(50)*/;

        if (!empty($request->from_date)) {
            $data = $data->whereBetween(DB::raw('DATE(working_days.date_working)'), array($request->from_date, $request->to_date));
        }

        return Datatables::of($data)
            ->filterColumn('working_date', function ($query, $keyword) {
                $query->whereRaw("DATE_FORMAT(working_days.date_working, '%d/%m/%Y') like ?", ["%{$keyword}%"]);
            })
            ->filterColumn('order_date', function ($query, $keyword) {
                $query->whereRaw("DATE_FORMAT(orders.created_at, '%d/%m/%Y') like ?", ["%{$keyword}%"]);
            })
            ->filterColumn('order_time', function ($query, $keyword) {
                $query->whereRaw("DATE_FORMAT(orders.created_at, '%H:%i:%s') like ?", ["%{$keyword}%"]);
            })
            ->filterColumn('product', function ($query, $keyword) {
                $query->whereRaw("CONCAT(products.product_name,' ', products_attributes.color) like ?", ["%{$keyword}%"]);
            })
            ->filterColumn('districts', function ($query, $keyword) {
                $query->whereRaw("CONCAT(districts,' ', amphures,' ', provinces) like ?", ["%{$keyword}%"]);
            })
            ->make();
    }

    function pdfPerOrder($id)
    {
        $orders = Order::join('products', 'products.id', '=', 'orders.product_id')
            ->join('products_attributes', 'products_attributes.id', '=', 'orders.products_attributes_id')
            ->join('senders', 'senders.id', '=', 'orders.sender_id')
            ->where('orders.id', '=', $id)
            ->select(
                'orders.id',
                'senders.sender_name',
                'senders.description as sender_description',
                DB::raw("DATE_FORMAT(orders.updated_at, '%d/%m/%Y') AS order_date"),
                DB::raw("DATE_FORMAT(orders.updated_at, '%H:%i:%s') AS order_time"),
                'orders.name_surename',
                'orders.tel',
                'orders.address',
                'provinces AS provinces_name',
                'amphures AS amphures_name',
                'districts AS districts_name',
                'zipcode AS zip_code',
                'products.product_name',
                'products_attributes.color',
                'orders.payment',
                'orders.total_price',
                'orders.purchase_channel',
                'orders.profile',
                'orders.created_at',
                'orders.updated_at',
                'orders.deleted_at'
            )->orderBy('orders.updated_at', 'DESC')->paginate(50);

        Order::where('orders.id', '=', $id)
            ->update([
                'status_print' => 1
            ]);

        $pdf = PDF::loadView('backend.pdf.perorder', ['orders' => $orders])->setPaper('a4');
        return $pdf->stream();
    }

    function pdfselectedOrder(Request $request)
    {

        $data = $request->all();

        $orders = Order::join('products', 'products.id', '=', 'orders.product_id')
            ->join('products_attributes', 'products_attributes.id', '=', 'orders.products_attributes_id')
            ->join('senders', 'senders.id', '=', 'orders.sender_id')
            ->whereIn('orders.id', $data['checkitem'])
            ->select(
                'orders.id',
                'senders.sender_name',
                'senders.description as sender_description',
                DB::raw("DATE_FORMAT(orders.updated_at, '%d/%m/%Y') AS order_date"),
                DB::raw("DATE_FORMAT(orders.updated_at, '%H:%i:%s') AS order_time"),
                'orders.name_surename',
                'orders.tel',
                'orders.address',
                'provinces AS provinces_name',
                'amphures AS amphures_name',
                'districts AS districts_name',
                'zipcode AS zip_code',
                'products.product_name',
                'products_attributes.color',
                'orders.payment',
                'orders.total_price',
                'orders.purchase_channel',
                'orders.profile',
                'orders.created_at',
                'orders.updated_at',
                'orders.deleted_at'
            )
            ->orderBy('products.id', 'ASC')
            ->orderBy('products_attributes.id', 'ASC')
            ->get();

        Order::whereIn('orders.id', $data['checkitem'])
            ->update([
                'status_print' => 1
            ]);



        $pdf = PDF::loadView('backend.pdf.perday', ['orders' => $orders])->setPaper('a4');
        return $pdf->stream();
    }
}
