<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Order;
use DB;

class ReportOrdersController extends Controller
{
    function index(Request $request)
    {
        return view('backend.reportorders.index');
    }

    function indexData(Request $request)
    {

            DB::connection()->enableQueryLog();

            $user = Auth::user()->getRoleNames();

            $data = Order::join('products', 'products.id', '=', 'orders.product_id')
                ->join('products_attributes', 'products_attributes.id', '=', 'orders.products_attributes_id')
                ->join('working_days', 'working_days.id', '=', 'orders.working_days_id')
                ->select(
                    'orders.id',
                    DB::raw("DATE_FORMAT(working_days.date_working, '%d/%m/%Y') AS working_date"),
                    DB::raw("DATE_FORMAT(orders.created_at, '%d/%m/%Y') AS order_date"),
                    DB::raw("DATE_FORMAT(orders.created_at, '%H:%i:%s') AS order_time"),
                    'orders.name_surename',
                    'orders.tel',
                    'orders.address',
                    DB::raw("CONCAT(districts,' ', amphures,' ', provinces) AS districts"),
                    'zipcode AS zip_code',
                    DB::raw("CONCAT(products.product_name,' ', products_attributes.color) AS product"),
                    'orders.payment',
                    'orders.total_price',
                    'orders.price_cost',
                    'orders.purchase_channel',
                    'orders.profile',
                    'orders.created_at',
                    'orders.updated_at',
                    'orders.deleted_at'
                )->orderBy('orders.id', 'DESC');

            if ($user[0] != 'admin') {
                $data = $data->where('created_user_id', '=', Auth::user()->id);
            }

            if (!empty($request->from_date)) {
                $data = $data->whereBetween(DB::raw('DATE(working_days.date_working)'), array($request->from_date, $request->to_date));

            }

            $data = $data->get();

            return datatables()->of($data)->make(true);
    }
}
