<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\StocksRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Product;
use App\ProductsAttribute;
use App\Stock;
use DB;

class StocksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function manage(Request $request, $id)
    {
        $products_attributes = Product::orderBy('products.id', 'ASC')
            ->join('products_attributes', 'products.id', '=', 'products_attributes.product_id')
            ->where('products_attributes.id', '=', $id)
            ->first();

        $data = Stock::where([
            ['products_attributes_id', '=', $id],
            ['stock_balance', '!=', 0],
        ])
        ->get();

        // dd($data);


        return view('backend.stocks.manage', compact('products_attributes', 'data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function create()
    // {
    // }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function store(Request $request)
    // {
    // }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function show($id)
    // {
    //     //
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // $data = Product::find($id);
        $data = Product::orderBy('products.id', 'ASC')
            ->where('products_attributes.id', '=', $id)
            ->join('products_attributes', 'products.id', '=', 'products_attributes.product_id')
            ->first();

        return view('backend.stocks.add', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(StocksRequest $request, $id)
    {

        $data = $request->all();

        // dd($data);

        $products_attributes = ProductsAttribute::find($id);

        ProductsAttribute::where(['id' => $id])
            ->update([
                'stock' => DB::raw('stock+' . $data['stock_add'])
            ]);

        $log_stock = new Stock;

        $log_stock->product_id                  = $products_attributes->product_id;
        $log_stock->products_attributes_id      = $products_attributes->id;
        $log_stock->stock_add                   = $data['stock_add'];
        $log_stock->stock_balance               = $data['stock_add'];
        $log_stock->price_cost                  = $data['price_cost'];

        $log_stock->created_user_id             = Auth::user()->id;

        $log_stock->save();

        return redirect()->route('products.index')
            ->with('success', 'Stocks Added Successfully!');
    }

    public function updateManage(Request $request)
    {

        $id = $request->pk;

        if($request->name == 'price_cost'){
            Stock::where(['id' => $id])
                ->update([
                    'price_cost' => $request->value
                ]);
        }else{
            $old_stock = Stock::find($id);
            // var_dump($old_stock);

            if ($request->value > $old_stock->stock_balance) {
                return 'ห้ามเพิ่มจำนวนคงเหลือ เนื่องจากสินค้าถูกขายไปแล้ว กรุณาไปเพิ่ม Stock!';
            }

            $difference_stock = $old_stock->stock_balance - $request->value; //จำนวนที่สินค้าที่หายไป

            Stock::where(['id' => $id])
                ->update([
                    'stock_balance' => $request->value
                ]);

            ProductsAttribute::where(['id' => $old_stock->products_attributes_id])
                ->update([
                    'stock' => DB::raw('stock-' . $difference_stock)
                ]);
        }




    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
