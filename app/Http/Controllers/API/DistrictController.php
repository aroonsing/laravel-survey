<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use DB;

class DistrictController extends Controller
{
    public function provinces()
    {
        $provinces = DB::table('provinces')
            ->orderBy('name_th', 'ASC')
            ->get();
        return response()->json($provinces);
    }
    public function amphoes($province_id)
    {
        $amphoes = DB::table('amphures')
            ->where('name_th', 'not like', '%*%')
            ->where('province_id', $province_id)
            ->orderBy('name_th', 'ASC')
            ->get();

        return response()->json($amphoes);
    }
    public function districts($amphure_id)
    {
        $districts = DB::table('districts')
            ->where('name_th', 'not like', '%*%')
            ->where('amphure_id', $amphure_id)
            ->orderBy('name_th', 'ASC')
            ->get();
        return response()->json($districts);
    }
}
