<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ProductsAttribute;

class ProductController extends Controller
{

    public function products_attributes($product_id)
    {
        $products_attributes = ProductsAttribute::where('product_id', '=', $product_id)->get();

        return response()->json($products_attributes);
    }
}
