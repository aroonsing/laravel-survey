<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

// Auth::routes();

Route::get('/', function () {
    return view('backend.dashboard.index');
})->middleware('auth');

Route::match(['get', 'post'], 'login', ['as' => 'login', 'uses' => 'Auth\LoginController@signin']);

// Route::match(['get', 'post'], '/signin   ', 'Auth\LoginController@signin');

Route::get('/logout', 'Auth\LoginController@logout');

Route::group(['middleware' => ['auth']], function () {
    //Login, ChangePassword, Setting Admin
    Route::get('/backend/dashboard', 'Backend\DashboardController@Index');
    Route::get('/auth/settings', 'Auth\LoginController@settings');
    Route::get('/auth/check-pwd', 'Auth\LoginController@chkPassword');
    Route::match(['get', 'post'], '/auth/update-password', 'Auth\LoginController@updatePassword');
    Route::match(['get', 'post'], '/signout', 'Auth\LoginController@Signout');

    Route::match(['get', 'post'], '/backend/working-days/data', 'Backend\WorkingDaysController@indexData');
    Route::resource('/backend/working-days', 'Backend\WorkingDaysController');

    Route::resource('/backend/admins', 'Backend\AdminsController');
    Route::resource('/backend/partners', 'Backend\PartnersController');
    Route::match(['get', 'post'], '/backend/senders/data', 'Backend\SendersController@indexData');
    Route::resource('/backend/senders', 'Backend\SendersController');

    Route::match(['get', 'post'], '/backend/products/data', 'Backend\ProductsController@indexData');
    Route::match(['get', 'post'], '/backend/products/details-data/{id}', 'Backend\ProductsController@indexDataDetail');
    Route::resource('/backend/products', 'Backend\ProductsController');
    Route::get('/backend/products/delete-image/{id}', 'Backend\ProductsController@deleteImage');
    Route::match(['get', 'post'], '/backend/products/add-attributes/{id}', 'Backend\ProductsController@addAttributes');
    Route::match(['get', 'post'], '/backend/products/delete-attribute/{id}', 'Backend\ProductsController@deleteAttribute');

    Route::match(['get', 'post'], '/backend/stocks/manage/update', 'Backend\StocksController@updateManage')->name('stocks.manage.update');
    Route::match(['get', 'post'], '/backend/stocks/manage/{id}', 'Backend\StocksController@manage')->name('stocks.manage');
    Route::resource('/backend/stocks', 'Backend\StocksController');

    Route::match(['get', 'post'], '/backend/orders/data', 'Backend\OrdersController@indexData');
    Route::resource('/backend/orders', 'Backend\OrdersController');

    Route::match(['get', 'post'], '/backend/claim-orders/data/{id}', 'Backend\ClaimOrdersController@showData');
    Route::match(['get', 'post'], '/backend/claim-orders/{id}/create/', 'Backend\ClaimOrdersController@create')->name('claim-orders.create');
    Route::resource('/backend/claim-orders', 'Backend\ClaimOrdersController')->only([
        'store', 'show', 'edit', 'update', 'destroy',
    ]);;

    /**
     * Print Orders
     */
    Route::match(['get', 'post'], '/backend/printorders/', 'Backend\PrintOrdersController@index');
    Route::match(['get', 'post'], '/backend/printorders/data', 'Backend\PrintOrdersController@indexData');

    Route::get('/backend/pdf/print-per-orders/{id}', 'Backend\PrintOrdersController@pdfPerOrder');
    Route::match(['get', 'post'], '/backend/pdf/print-selected-orders', 'Backend\PrintOrdersController@pdfselectedOrder')->name('print-selected-orders');

    /**
     * Print Claim Orders
     */
    Route::match(['get', 'post'], '/backend/printclaimorders/', 'Backend\PrintClaimOrdersController@index');
    Route::match(['get', 'post'], '/backend/printclaimorders/data', 'Backend\PrintClaimOrdersController@indexData');

    Route::get('/backend/pdf/printclaim-per-orders/{id}', 'Backend\PrintClaimOrdersController@pdfPerOrder');
    Route::match(['get', 'post'], '/backend/pdf/print-selected-claimorders', 'Backend\PrintClaimOrdersController@pdfselectedOrder')->name('print-selected-claimorders');

    // Route::resource('/backend/reportorders', 'Backend\ReportOrdersController');
    Route::match(['get', 'post'], '/backend/reportorders', 'Backend\ReportOrdersController@index')->name('reportorders.index');
    Route::match(['get', 'post'], '/backend/reportorders/data', 'Backend\ReportOrdersController@indexData')->name('reportorders.indexData');

    Route::match(['get', 'post'], '/backend/report-claimorders', 'Backend\ReportClaimOrdersController@index')->name('report-claimorders.index');
    Route::match(['get', 'post'], '/backend/report-claimorders/data', 'Backend\ReportClaimOrdersController@indexData')->name('report-claimorders.indexData');
});
